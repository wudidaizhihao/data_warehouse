select * from (
select t1.employee_id 
,t1.company_name 
,t2.company_name 
,t1.company_code 
,t1.job_status 
,row_number () over (partition by t1.employee_id order by t1.position_start_date desc )  num 
from hr_test.hr_work_experience_inner t1 
join hr_test.js_sys_company t2 on t1.company_code  = t2.company_code 
where t1.job_status in ('1','4') 
) t3 where num = 1

--查看员工id有多条的数据
select * from  hr_test.hr_work_experience_inner  t2 where t2.employee_id in (
select employee_id from hr_test.hr_work_experience_inner t1  group by t1.employee_id 
having count(1) >1 )

--取position_start_date 职位开始时间最新的
select t1.employee_id ,t1.company_name ,t1.position_start_date  ,row_number () over (partition by employee_id order by position_start_date desc )  num 
from hr_test.hr_work_experience_inner  t1

--取position_start_date 职位开始时间最新的
create table hr_test.companies_not_match  as 

----获取匹配上的公司
insert into   hr_test.companies_not_match_dim
select   company_name_3
,full_name 
,full_name_3 
,dept_name 
,country 
,province 
,city 
,county  from (
select t3.company_name  company_name_3
,t3.full_name 
,t3.full_name full_name_3 
,t4.dept_name 
,t4.country 
,t4.province 
,t4.city 
,t4.county  from  (
select t1.* 
,row_number () over (partition by employee_id order by position_start_date desc )  num
from hr_test.hr_work_experience_inner  t1  where t1.job_status in ('1','4') ) t2 
join  hr_test.js_sys_company t3 on t2.company_code  = t3.company_code 
left join dim.dim_property_department t4 on t3.full_name  = t4.dept_name 
where  t2.num = 1   

) t5 where dept_name  is not null  
group by  company_name_3
,full_name 
,full_name_3 
,dept_name 
,country 
,province 
,city 
,county 

---匹配对不上的
create  table hr_test.companies_not_match_dim as 
select company_name_3 
,full_name 
,replace (full_name_2,'）',')') full_name_3
, t3.dept_name 
, t3.country 
,t3.province 
,t3.city 
,t3.county 
from (
select company_name_3,full_name ,replace (full_name,'（','(') full_name_2 
from hr_test.companies_not_match  ) t2 left join  dim.dim_property_department t3
on replace (full_name_2,'）',')') = concat(t3.dept_name,'本部') 

--人才地图

select count(1) from hr_test.hr_talent_map

---- 
drop table hr_test.ads_hr_talent_map

create table hr_test.ads_hr_talent_map as 
select t3.company_name
,t4.country 
,t4.province 
,t4.city
,t4.county 
,t4.full_name 
,t4.dept_name 
,t3.employee_id 
,t3.employee_name 
,t3.company_code 
,t3.job_status 
,'' longitude 
,'' dimension 
from (
select t1.employee_id 
,t2.company_name 
,t1.extend_field_1 employee_name
,t1.company_code 
,t1.job_status 
,row_number () over (partition by t1.employee_id order by t1.position_start_date desc )  num 
from hr_test.hr_work_experience_inner t1 
join hr_test.js_sys_company t2 on t1.company_code  = t2.company_code 
where t1.job_status in ('1','4') 
) t3 join companies_match_dim t4  on t3.company_name  = t4.company_name_3 
where t3.num = 1

--根据full_name 更新
update hr_test.ads_hr_talent_map 
set longitude  = hr_test.ads_hr_talent_map  .longitude ,
dimension  = hr_test .ads_hr_talent_map.dimension 
from  hr_test .companies_match_dim
where hr_test.ads_hr_talent_map .full_name  
 = hr_test .companies_match_dim.full_name

select * from hr_test.companies_match_dim cmd 









--64396

select count(distinct t1.employee_id) from hr_employee t1 
join hr_test.hr_work_experience_inner t2 
on t1.employee_id = t2.employee_id 
where t2.job_status in ('1','4')

hr_test.hr_work_experience_inner: 57329

hr_employee :57326



