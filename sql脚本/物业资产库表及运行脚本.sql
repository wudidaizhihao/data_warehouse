-- 物业资产规模排名:根据实际面积,物业单位,统计面积规模
-- 物业租金均价排名：筛选出已出租的物业，物业均价=同一个物业【合同涉及金额】加总／同一个物业（出租面积*时间区间）加总，时间区间=租赁合同结束时间-租赁合同开始时间
-- 物业使用情况分布：统计各单位自用物业面积，表字段用【实际面积】计算、状态维度【物业使用情况】、使用状态【物业使用情况】
-- 物业业态功能分布: 统计各单位各业态物业面积，表字段用【实际面积】计算、公司维度【实控单位】、业态功能维度【功能分类】
--资本运营中心_集团物业资产

CREATE TABLE capital.ads_property_stateproperty_total_month (
	id int8 NOT NULL, -- 主键id
	bloc_name text NULL, -- 集团名称
	control_unit text NULL, -- 实控单位
	property_name text NULL, -- 物业名称
	province text NULL, -- 省份
	city text NULL, -- 所属城市
	county text NULL, -- 所属区县
	street text NULL, -- 所属街道
	address text NULL, -- 物业地址
	confirmed text NULL, -- 是否确权
	area_load numeric NULL, -- 证载面积
	area_actual numeric NULL, -- 实际面积
	utility_load text NULL, -- 证载用途
	utility_actual text NULL, -- 实际用途
	mortgage text NULL, -- 是否有抵押
	litigation text NULL, -- 是否有诉讼
	wholly text NULL, -- 是否全资
	property_category text NULL, -- 物业类别
	remark text NULL, -- 备注
	tenement_usage text NULL, -- 物业使用情况
	tenement_address text NULL, -- 详细地址
	functional text NULL, -- 功能分类
	tenement_actual text NULL, -- 物业实际用途
	tenement_area text NULL, -- 物业实际面积
	trading_platform text NULL, -- 交易平台编号
	lease_way text NULL, -- 招租方式
	approval_units text NULL, -- 批准单位
	rental_purposes text NULL, -- 出租用途
	rental_category text NULL, -- 出租类别
	housingrental_area numeric NULL, -- 房产出租面积
	leasehold_area numeric NULL, -- 土地出租面积
	area_address text NULL, -- 所在地址
	unitrent_price numeric NULL, -- 房产租金单价
	arearent_price numeric NULL, -- 土地租金单价
	total_price text NULL, -- 总租金
	rent_formulation text NULL, -- 租金拟定方式
	totalreference_price text NULL, -- 合同期评估_参考价总额
	lessee text NULL, -- 承租方
	lessee_type text NULL, -- 承租方类型
	start_time text NULL, -- 租赁合同开始时间
	end_time text NULL, -- 租赁合同结束时间
	monthly_price numeric NULL, -- 月租金单价
	contract_amount numeric NULL, -- 合同涉及金额
	outstanding_rent numeric NULL, -- 欠缴租金
	recovered_rent numeric NULL, -- 已追缴租金合计
	remaining_rent numeric NULL, -- 当前剩余未追缴租金
	litigation_related text NULL, -- 是否有租赁相关的诉讼
	litigation_result text NULL, -- 诉讼结果
	reason text NULL, -- 原因说明
	overdue text NULL, -- 是否有逾期占用情况
	idle_category text NULL, -- 闲置类别
	property_status text NULL, -- 物业现状
	idle_starttime text NULL, -- 闲置起始时间
	release_time text NULL, -- 最近一次交易平台发布信息时间
	idle_reasons text NULL, -- 闲置原因及存在问题
	idle_remark text NULL, -- 闲置备注
	longitude varchar NULL, -- 物业地址经度
	latitude varchar NULL, -- 物业地址维度
	create_time timestamp NULL DEFAULT now(), -- 创建时间
	abbreviation varchar NULL -- 公司简称
);
COMMENT ON TABLE capital.ads_property_stateproperty_total_month IS '物业_国资物业情况';

-- Column comments

COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.id IS '主键id';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.bloc_name IS '集团名称';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.control_unit IS '实控单位';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.property_name IS '物业名称';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.province IS '省份';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.city IS '所属城市';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.county IS '所属区县';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.street IS '所属街道';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.address IS '物业地址';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.confirmed IS '是否确权';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.area_load IS '证载面积';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.area_actual IS '实际面积';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.utility_load IS '证载用途';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.utility_actual IS '实际用途';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.mortgage IS '是否有抵押';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.litigation IS '是否有诉讼';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.wholly IS '是否全资';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.property_category IS '物业类别';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.remark IS '备注';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.tenement_usage IS '物业使用情况';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.tenement_address IS '详细地址';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.functional IS '功能分类';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.tenement_actual IS '物业实际用途';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.tenement_area IS '物业实际面积';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.trading_platform IS '交易平台编号';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.lease_way IS '招租方式';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.approval_units IS '批准单位';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.rental_purposes IS '出租用途';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.rental_category IS '出租类别';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.housingrental_area IS '房产出租面积';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.leasehold_area IS '土地出租面积';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.area_address IS '所在地址';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.unitrent_price IS '房产租金单价';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.arearent_price IS '土地租金单价';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.total_price IS '总租金';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.rent_formulation IS '租金拟定方式';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.totalreference_price IS '合同期评估_参考价总额';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.lessee IS '承租方';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.lessee_type IS '承租方类型';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.start_time IS '租赁合同开始时间';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.end_time IS '租赁合同结束时间';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.monthly_price IS '月租金单价';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.contract_amount IS '合同涉及金额';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.outstanding_rent IS '欠缴租金';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.recovered_rent IS '已追缴租金合计';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.remaining_rent IS '当前剩余未追缴租金';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.litigation_related IS '是否有租赁相关的诉讼';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.litigation_result IS '诉讼结果';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.reason IS '原因说明';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.overdue IS '是否有逾期占用情况';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.idle_category IS '闲置类别';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.property_status IS '物业现状';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.idle_starttime IS '闲置起始时间';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.release_time IS '最近一次交易平台发布信息时间';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.idle_reasons IS '闲置原因及存在问题';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.idle_remark IS '闲置备注';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.longitude IS '物业地址经度';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.latitude IS '物业地址维度';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.create_time IS '创建时间';
COMMENT ON COLUMN capital.ads_property_stateproperty_total_month.abbreviation IS '公司简称';


-- DROP TABLE capital.ads_property_average_rent_total_month;

CREATE TABLE capital.ads_property_average_rent_total_month (
	id int8 NULL, -- 序号
	property_name varchar(500) NULL, -- 物业名称
	monthly_rent float8 NULL, -- 月度租金
	province varchar(500) NULL, -- 省份
	city varchar(500) NULL, -- 城市
	county varchar(500) NULL, -- 区县
	abbreviation varchar(500) NULL, -- 公司简称
	property_category varchar(500) NULL, -- 物业类别
	contract_amount_total numeric NULL, -- 总租金_元
	housingrental_area_total float8 NULL, -- 出租面积_平方
	date_total numeric null,
	start_time date NULL, -- 合同开始时间
	end_time date NULL, -- 合同结束时间
	update_time date NULL -- 更新时间
);
COMMENT ON TABLE capital.ads_property_average_rent_total_month IS '物业租金排名';

-- Column comments

COMMENT ON COLUMN capital.ads_property_average_rent_total_month.id IS '序号';
COMMENT ON COLUMN capital.ads_property_average_rent_total_month.property_name IS '物业名称';
COMMENT ON COLUMN capital.ads_property_average_rent_total_month.monthly_rent IS '月度租金';
COMMENT ON COLUMN capital.ads_property_average_rent_total_month.province IS '省份';
COMMENT ON COLUMN capital.ads_property_average_rent_total_month.city IS '城市';
COMMENT ON COLUMN capital.ads_property_average_rent_total_month.county IS '区县';
COMMENT ON COLUMN capital.ads_property_average_rent_total_month.abbreviation IS '公司简称';
COMMENT ON COLUMN capital.ads_property_average_rent_total_month.property_category IS '物业类别';
COMMENT ON COLUMN capital.ads_property_average_rent_total_month.contract_amount_total IS '总租金_元';
COMMENT ON COLUMN capital.ads_property_average_rent_total_month.housingrental_area_total IS '总出租面积_平方';
COMMENT ON COLUMN capital.ads_property_average_rent_total_month.date_total IS '总月份';
COMMENT ON COLUMN capital.ads_property_average_rent_total_month.start_time IS '合同开始时间';
COMMENT ON COLUMN capital.ads_property_average_rent_total_month.end_time IS '合同结束时间';
COMMENT ON COLUMN capital.ads_property_average_rent_total_month.update_time IS '更新时间';

-- Permissions

ALTER TABLE capital.ads_property_average_rent_total_month OWNER TO prodba;


--物业租金排行
--使用公式:筛选出已出租的物业，物业均价=同一个物业【合同涉及金额】加总／同一个物业（出租面积*时间区间）加总，时间区间=租赁合同结束时间-租赁合同开始时间

insert into capital."ads_property_average_rent_total_month" 

--grant select on capital."ads_property_average_rent_total_month" to grgdba
--create table capital."ads_property_average_rent_total_month"  as
select row_number () over() id 
    ,t2.property_name  
    ,sum(t2.contract_amount)/sum(t2.housingrental_area*year_total*12) monthly_rent  
	,t2.province
	,t2.city
	,t2.county 
	,t2.abbreviation 
	,t2.property_category
	,sum(t2.contract_amount) contract_amount_total
	,sum(t2.housingrental_area*year_total*12) housingrental_area_total
	,sum(year_total*12) date_total 
	,t2.start_time
	,t2.end_time
from 
  (select t1.property_name  --物业名称
	,t1.housingrental_area --房产出租面积
	,t1.province
	,t1.city
	,t1.county
	,t1.abbreviation
	,t1.property_category
	,extract  (year from age(to_date(t1.end_time,'yyyy-MM-dd') --计算开始时间至结束时间,之间的月份数
	,to_date(t1.start_time ,'yyyy-MM-dd'))) +1 year_total
	,to_date(t1.start_time,'yyyy-MM-dd') start_time --租赁合同开始时间
	,to_date(t1.end_time,'yyyy-MM-dd') end_time  --租赁合同结束时间
	,t1.contract_amount  --合同涉及金额 --或者使用 totalreference_price 合同参考金额
from capital.ads_property_stateproperty_total_month t1
where t1.tenement_usage = '出租' and t1.contract_amount is not null
) t2  group by t2.property_name  --房产出租面积
	,t2.province
	,t2.city
	,t2.county
	,t2.abbreviation
	,t2.property_category
	,t2.start_time
	,t2.end_time
	





--物业资产排名(有误,后面把2级单位更新到物业表里,通过bi工具进行统计,不使用该表)
select sum(t4.area_actual) area_actual,t5.abbreviation  from 
(
select t1.id  
	,t1.control_unit --实控单位
	,t1.area_actual --实际面积
	,t2.dept_id --公司id
	,t3.parent_dept_id  --上级id
	,t3.parent_dept_level --上级层级
	,t3.child_dept_level  --本级层级
from capital.ads_property_stateproperty_total_month t1
join dim.dim_property_department t2 
on t1.control_unit  = t2.dept_name 
join  dim.dim_property_department_relation_recurve   t3
on t3.child_dept_id  = t2.dept_id  where t3.parent_dept_level  = 2 
--or t3.parent_dept_level = 1
) t4 join    dim.dim_property_department t5 
on t4.parent_dept_id = t5.dept_id 
group by abbreviation



--赋权
grant select on capital .ads_property_use_status_total_years to grgdba
--物业使用状态分布 ,(后面把简称加到了物业表中,通过bi工具自行展示,不使用该表)
delete from capital .ads_property_use_status_total_years
insert into capital .ads_property_use_status_total_years
select row_number () over() id,control_unit,area_actual,tenement_usage,abbreviation from (
select  control_unit, area_actual,tenement_usage,t5.abbreviation  from 
(
select t1.id  
	,t1.control_unit  --实控单位
	,t1.area_actual --实际面积
	,t1.tenement_usage --物业使用情况
	,t2.dept_id --公司id
	,t3.parent_dept_id  --上级id
	,t3.parent_dept_level --上级层级
	,t3.dept_level  --本级层级
from capital.ads_property_stateproperty_total_month t1
join dim.dim_property_department t2 
on t1.control_unit  = t2.dept_name 
join  dim.dim_department_relation_recurve t3
on t3.dept_id  = t2.dept_id  where t3.parent_dept_level  = 2 
) t4 join    dim.dim_property_department t5 
on t4.parent_dept_id = t5.dept_id 

union all 
select control_unit,area_actual ,tenement_usage,'无线电集团' abbreviation from 
capital.ads_property_stateproperty_total_month t6 where control_unit = '广州无线电集团有限公司'
) t7



--赋权
grant select on capital .ads_property_function_distribution_total_year to grgdba
--物业业态功能分布 ,(后面把简称加到了物业表中,通过bi工具自行展示,不使用该表)
delete from capital .ads_property_function_distribution_total_year
(后面把简称加到了物业表中,通过bi工具自行展示)
insert into capital .ads_property_function_distribution_total_year
select row_number () over() id,control_unit,area_actual,tenement_actual,abbreviation from (
select  control_unit, area_actual,tenement_actual,t5.abbreviation  from 
(
select t1.id  
	,t1.control_unit --实控单位
	,t1.area_actual --实际面积
	,t1.tenement_actual --物业实际用途
	,t2.dept_id --公司id
	,t3.parent_dept_id --上级id
	,t3.parent_dept_level  --上级层级
	,t3.dept_level --本级层级
from capital.ads_property_stateproperty_total_month t1
join dim.dim_property_department t2 
on t1.control_unit  = t2.dept_name 
join  dim.dim_department_relation_recurve t3
on t3.dept_id  = t2.dept_id  where t3.parent_dept_level  = 2 
) t4 join    dim.dim_property_department t5 
on t4.parent_dept_id = t5.dept_id 

union all 
select control_unit,area_actual ,tenement_actual,'无线电集团' abbreviation from 
capital.ads_property_stateproperty_total_month t6 where control_unit = '广州无线电集团有限公司'
) t7


--更新物业别名(默认更新到2级单位,将 3,4,5 级单位,统一使用2级单位的简称)
update capital.ads_property_stateproperty_total_month 
set abbreviation = t8.abbreviation
from 
(select id
	,control_unit  --实控单位
	,abbreviation  --简称
from 
(
select t1.id
	,t1.control_unit  --实控单位
	,t2."level" --层级
	,t3.parent_dept_id   --上级id
from capital.ads_property_stateproperty_total_month t1 
join dim.dim_property_department t2 on t1.control_unit  = t2.dept_name 
join  dim.dim_department_relation_recurve t3 on t3.dept_id  = t2.dept_id 
where t3.parent_dept_level  = 2 
) t4 join    dim.dim_property_department t5 
on t4.parent_dept_id = t5.dept_id 

union all
select t1.id
	,t1.control_unit --实控单位
	,t2.abbreviation --简称
from capital.ads_property_stateproperty_total_month t1 
join dim.dim_property_department t2 on t1.control_unit  = t2.dept_name 
where t2."level" = 2 or t2."level" =1
) t8
where t8.id  = ads_property_stateproperty_total_month.id 


--更新产业园别名(默认更新到2级单位,将 3,4,5 级单位,统一使用2级单位的简称)
update capital.ads_property_group_park_information_total_year  
set abbreviation = t8.abbreviation
from 
(select id,enterprise_name ,abbreviation from 
(
select t1.id
	,t1.enterprise_name --用地权属企业名称
	,t2."level" --层级
	,t3.parent_dept_id  --父级id
from capital.ads_property_group_park_information_total_year  t1 
join dim.dim_property_department t2 on t1.enterprise_name  = t2.dept_name 
join  dim.dim_department_relation_recurve t3 on t3.dept_id  = t2.dept_id 
where t3.parent_dept_level  = 2 
) t4 join  dim.dim_property_department t5 on t4.parent_dept_id = t5.dept_id 

union all
select t1.id
	 ,t1.enterprise_name --用地权属企业名称
	 ,t2.abbreviation --用户简称
 from capital.ads_property_group_park_information_total_year   t1 
join dim.dim_property_department t2 on t1.enterprise_name  = t2.dept_name 
where t2."level" = 2 or t2."level" =1
) t8
where t8.id  = ads_property_group_park_information_total_year.id 


##更新物业基本信息脚本执行
##步骤一:
update  capital.ads_property_basic_total_month
set province =  btrim(province, '省') where province like '%省'
##步骤二:
update   capital.ads_property_basic_total_month
set province =  '内蒙古' where province = '内蒙古自治区'

##步骤三:
update   capital.ads_property_basic_total_month
set province =  '新疆' where province = '新疆维吾尔自治区'

##步骤四:
update   capital.ads_property_basic_total_month
set province =  '北京' where province = '北京市'

##步骤五:
update   capital.ads_property_basic_total_month
set province =  '广西' where province = '广西壮族自治区'

----------更新市字段
##步骤六:
update  capital.ads_property_basic_total_month
set city =  btrim(city, '市') where city like '%市'

##步骤七:
update   capital.ads_property_basic_total_month
set city =  '巴音郭楞' where city = '巴音郭楞蒙古自治州'

---------更新区字段

##步骤八:
update  capital.ads_property_basic_total_month
set county =  btrim(county, '区') where county like '%区'

##步骤九:
update  capital.ads_property_basic_total_month
set county =  '阿左旗'  where county = '阿拉善左旗'

##步骤九:
update  capital.ads_property_basic_total_month
set county =  '阿左旗'  where county = '阿拉善左旗'

##步骤十:
update  capital.ads_property_basic_total_month
set county =  '乌中旗'  where county = '乌拉特中旗'

##步骤十:
update  capital.ads_property_basic_total_month
set county =  '额济纳'  where county = '额济纳旗'

##步骤十一:
update  capital.ads_property_basic_total_month
set county =  '锡林浩特'  where county = '锡林浩特市'

##步骤十二:
update  capital.ads_property_basic_total_month
set county =  '西乌旗'  where county = '西乌珠穆沁旗'

##步骤十三:
update  capital.ads_property_basic_total_month
set county =  '库尔勒'  where county = '库尔勒市'

##步骤十四:
update  capital.ads_property_basic_total_month
set county =  '五原'  where county = '五原县'

##步骤十五:
update capital.ads_property_basic_total_month 
set control_unit = 
trim(both from regexp_replace(capital.ads_property_basic_total_month
.control_unit::varchar, '[\n\r,()" ]+', '', 'g'))

##步骤十六：
update capital.ads_property_basic_total_month 
set abbreviation = t8.abbreviation
from 
(select id
	,control_unit  --实控单位
	,abbreviation  --简称
from 
(
select t1.id
	,t1.control_unit  --实控单位
	,t2."level" --层级
	,t3.parent_dept_id   --上级id
from capital.ads_property_basic_total_month t1 
join dim.dim_property_department t2 on t1.control_unit  = t2.dept_name 
join  dim.dim_property_department_relation_recurve t3 on t3.child_dept_id  = t2.dept_id 
where t3.parent_dept_level  = 2 
) t4 join    dim.dim_property_department t5 
on t4.parent_dept_id = t5.dept_id 

union all
select t1.id
	,t1.control_unit --实控单位
	,t2.abbreviation --简称
from capital.ads_property_basic_total_month t1 
join dim.dim_property_department t2 on t1.control_unit  = t2.dept_name 
where t2."level" = 2 or t2."level" =1
) t8
where t8.id  = ads_property_basic_total_month.id 


##更新土地信息简称

update capital.ads_property_land_assets_total_year 
set enterprise_name = 
trim(both from regexp_replace(capital.ads_property_land_assets_total_year
.enterprise_name::varchar, '[\n\r," ]+', '', 'g'))

update ads_property_land_assets_total_year
set enterprise_name = replace(replace(enterprise_name,'（', '('), '）', ')');

update capital.ads_property_land_assets_total_year 
set abbreviation = t8.abbreviation
from 
(select serial_number
	,enterprise_name  --实控单位
	,abbreviation  --简称
from 
(
select t1.serial_number
	,t1.enterprise_name  --实控单位
	,t2."level" --层级
	,t3.parent_dept_id   --上级id
from capital.ads_property_land_assets_total_year t1 
join dim.dim_property_department t2 on t1.enterprise_name  = t2.dept_name 
join  dim.dim_property_department_relation_recurve t3 on t3.child_dept_id  = t2.dept_id 
where t3.parent_dept_level  = 2 
) t4 join    dim.dim_property_department t5 
on t4.parent_dept_id = t5.dept_id 

union all
select t1.serial_number
	,t1.enterprise_name --实控单位
	,t2.abbreviation --简称
from capital.ads_property_land_assets_total_year t1 
join dim.dim_property_department t2 on t1.enterprise_name  = t2.dept_name 
where t2."level" = 2 or t2."level" =1
) t8
where t8.serial_number  = ads_property_land_assets_total_year.serial_number 

##更新物业使用信息别称

--1.中文括号转英文
update ads_property_stateproperty_total_month
set control_unit = replace(replace(control_unit,'（', '('), '）', ')');

--2.去除换行符
update capital.ads_property_basic_total_month 
set control_unit = 
trim(both from regexp_replace(capital.ads_property_basic_total_month
.control_unit::varchar, '[\n\r,()" ]+', '', 'g'))

--3.更新别称
update capital.ads_property_stateproperty_total_month 
set abbreviation = t8.abbreviation
from 
(select id
	,control_unit  --实控单位
	,abbreviation  --简称
from 
(
select t1.id
	,t1.control_unit  --实控单位
	,t2."level" --层级
	,t3.parent_dept_id   --上级id
from capital.ads_property_stateproperty_total_month t1 
join dim.dim_property_department t2 on t1.control_unit  = t2.dept_name 
join  dim.dim_property_department_relation_recurve t3 on t3.child_dept_id  = t2.dept_id 
where t3.parent_dept_level  = 2 
) t4 join    dim.dim_property_department t5 
on t4.parent_dept_id = t5.dept_id 

union all
select t1.id
	,t1.control_unit --实控单位
	,t2.abbreviation --简称
from capital.ads_property_stateproperty_total_month t1 
join dim.dim_property_department t2 on t1.control_unit  = t2.dept_name 
where t2."level" = 2 or t2."level" =1
) t8
where t8.id  = ads_property_stateproperty_total_month.id  

##记得将无线电集团改成 集团公司
