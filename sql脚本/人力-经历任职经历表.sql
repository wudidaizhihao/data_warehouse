
-- drop table if exists hr_ads.ads_hr_work_experience_inner_employee_info
--update  hr_dai.hr_work_experience_inner set company_code  = 'GD01008003000' where company_code  = 'GD0100800'  --该表GD01008003000 广州广电新兴产业园投资有限公司本部，同一成GD0100800
create table hr_ads.ads_hr_work_experience_inner_employee_info as
select t1.employee_id -- 员工编号
	-- 2022-05-23 BI端提出要求emp_code使用产权树的code,数据过滤需要用code处理
	,t15.emp_code as emp_code -- emp_code
	,t15.dept_id as company_code -- 公司编码
	,t15.parent_id -- 企业父编码
	,case when t15.abbreviation is null then '其他' else t15.abbreviation end as abbreviation -- 企业简称
	,case when t15.dept_name is null then '其他' else t15.dept_name end as company_name -- 公司名称
    ,t1.extend_field_1 as emp_name -- 姓名
	,case when t2.gender=0 then '女' when t2.gender=1 then '男' else '未' end as gender -- 性别
	,t5.name as ethic -- 民族
	,t6.status as have_child -- 生育情况
	,case when t4.name is null then '其他' else t4.name end as education -- 学历
	,t20.name degree --学位
	,t3.graduate_from_other --其他院校
	,case when t3.graduate_from='9999' then t3.graduate_from_other else t8.university_name end as graduate_name -- 毕业学校 t3.graduate_from 关联学校获取学校名称
	,t3.startdt education_start_time --学历开始时间
	,t3.enddt education_end_time  --学历结束时间 
	,t3.edu_major --专业
	,case when t8.university_level='211' then '211学校' when t8.university_level='985' then '985学校' else '其他' end as graduate_type -- 学校类型 应该同上 毕业学校
	,case when t11.post_type_name is null then '其他' else t11.post_type_name end as post_type_name -- 岗位类型/职能分类
	,case when t11.parent_post_type_name is null then '其他' else t11.parent_post_type_name end as parent_post_type_name -- 岗位大类
	,case when t14.name is null then '其他' else t14.name end as profession -- 职业级别
	,case when t12.name is null then '其他' else t12.name end as professional_title -- 职称级别
	,t2.birth -- 出生日期
	,t1.position_start_date join_date -- 入职日期
	,t1.position_end_date end_date --职位结束日期
	,t2.work_date -- 参加工作日期
	,age -- 年龄
	,date_part('day',current_date::timestamp-t2.join_date::timestamp)/365 as join_age -- 司龄
	,date_part('day',current_date::timestamp-t2.work_date::timestamp)/365 as work_age -- 工龄
	,case when t2.age<=25 then '25岁以下' 
		when t2.age>25 and t2.age<=30 then '26-30岁' 
		when t2.age>30 and t2.age<=35 then '31-35岁'
		when t2.age>35 and t2.age<=40 then '36-40岁'
		when t2.age>40 and t2.age<=45 then '41-45岁'
		when t2.age>45 and t2.age<=50 then '46-50岁'
		when t2.age>50 then '50岁以上' else '其他' end as age_grp -- 年龄段
	,case when date_part('day',current_date::timestamp-t2.join_date::timestamp)/365<1 then '1年以下'
		when date_part('day',current_date::timestamp-t2.join_date::timestamp)/365>=1 and date_part('day',current_date::timestamp-t2.join_date::timestamp)/365<=3 then '1-3年'
		when date_part('day',current_date::timestamp-t2.join_date::timestamp)/365>3 and date_part('day',current_date::timestamp-t2.join_date::timestamp)/365<=5 then '4-5年'
		when date_part('day',current_date::timestamp-t2.join_date::timestamp)/365>5 and date_part('day',current_date::timestamp-t2.join_date::timestamp)/365<=10 then '6-10年'
		when date_part('day',current_date::timestamp-t2.join_date::timestamp)/365>10 and date_part('day',current_date::timestamp-t2.join_date::timestamp)/365<=15 then '11-15年'
		when date_part('day',current_date::timestamp-t2.join_date::timestamp)/365>15 and date_part('day',current_date::timestamp-t2.join_date::timestamp)/365<=20 then '16-20年'
		when date_part('day',current_date::timestamp-t2.join_date::timestamp)/365>20 then '20年以上' else '其他' end as join_age_grp -- 司龄段
	,case when date_part('day',current_date::timestamp-t2.work_date::timestamp)/365<1 then '1年以下'
		when date_part('day',current_date::timestamp-t2.work_date::timestamp)/365>=1 and date_part('day',current_date::timestamp-t2.work_date::timestamp)/365<=3 then '1-3年'
		when date_part('day',current_date::timestamp-t2.work_date::timestamp)/365>3 and date_part('day',current_date::timestamp-t2.work_date::timestamp)/365<=5 then '4-5年'
		when date_part('day',current_date::timestamp-t2.work_date::timestamp)/365>5 and date_part('day',current_date::timestamp-t2.work_date::timestamp)/365<=10 then '6-10年'
		when date_part('day',current_date::timestamp-t2.work_date::timestamp)/365>10 and date_part('day',current_date::timestamp-t2.work_date::timestamp)/365<=15 then '11-15年'
		when date_part('day',current_date::timestamp-t2.work_date::timestamp)/365>15 and date_part('day',current_date::timestamp-t2.work_date::timestamp)/365<=20 then '16-20年'
		when date_part('day',current_date::timestamp-t2.work_date::timestamp)/365>20 then '20年以上' else '其他' end as work_age_grp -- 工龄段
	,t16.name  administrative_type--行政类型 
	,t17.employment_category_name --用工类别
	,case when t15.country is null then '其他' else t15.country end as country -- 国家
	,case when t15.province is null then '其他' else t15.province end as province -- 省份
	,case when t15.city is null then '其他' else t15.city end as city -- 地市
	,case when t15.county is null then '其他' else t15.county end as county -- 区县
	,t15.longitude -- 经度
	,t15.dimension as latitude -- 维度
	,t2.email --邮箱
	,t2.image_path --头像
	,case when t2.politic_face = '1' and t2.identity_id in (select id_number from politics_dwd.dwd_politics_party_member_detail_df) then '党员' 
        when t2.politic_face = '2' then '预备党员'
		when t2.politic_face = '3' then '团员'
		when t2.politic_face = '4' then '其他党派'	
		when t2.politic_face = '5' then '群众' else '其他党派' end as politic_face  --政治面貌
	,t2.local_party_branch -- 所属党支部
	,case when t1.alents = '0' then '否' 
	    when t1.alents = '1' then '是'
		else '其他' end as alents --是否高级人员
	,t1.second_department as department -- 部门
	,t1.job_name --职务
	,t18.entry_mode_name -- 招聘方式
	,t2.identity_id --证件号
	,t2.phone --手机号
	,t2.office_telephone --办公电话
	,t2.jiguan --籍贯
	,t2.registered_residence --户籍所在地
	,t21.party_member_job --党内职务
	,t21.party_join_date --入党时间
	,t21.party_regular_date -- 转正时间

	,t15.order_sort -- 排序号
	,case when t2.marital_status  = '0' then '未婚'
          when t2.marital_status  = '1' then '已婚'
          when t2.marital_status  = '2'	then '离异' else '其他' end as marital_status	  -- 婚姻情况 0 未婚 1 已婚 2 离异
	from 	--取工作经历表,根据工作状态1,4(在职,在岗) 取最新的position_start_date 进行筛选
     (select * from 
      (select t2.* ,row_number () over (partition by t2.employee_id,t2.company_code,position_start_date
        order by startdt DESC,position_start_date DESC )  num  from 
         hr_dai.hr_work_experience_inner  t2 ) t3 
           where t3.num  =1 )  t1
  
            
            
left join hr_dai.hr_employee t2 on t1.employee_id  = t2.employee_id               
left join hr_dai.companies_match_dim t15 on  t15.company_code  = t1 .company_code --用公司编码作匹配
left join hr_dai.hr_education_experience_handle t3 on t1.employee_id=t3.employee_id --and t3.stat_order =(select max(stat_order) from hr_dai.hr_education_experience_handle t20 where t1.employee_id = t20.employee_id ) --保留最高学历
left join hr_dai.hr_education_background t4 on t3.education=t4.id
left join hr_dai.hr_ethic t5 on t2.ethic=t5.ethic_id
left join hr_dai.hr_have_child t6 on t2.have_child=t6.id
left join hr_dai.hr_college_handle t8 on to_number(t3.second_graduate,'9999')=t8.coid
left join hr_dai.hr_post_type_handle t11 on t1.post_type=t11.id 
left join hr_dai.hr_current_status_handle t10 on t1.employee_id=t10.employee_id and 1=t10.stat_order
left join hr_dai.hr_contract_type t13 on t10.contract_type=t13.id 
left join hr_dai.hr_profession_skill_level t12 on t10.level_id=t12.id 
left join hr_dai.hr_licence_certification_level t14 on t10.license_level=t14.id
left join hr_dai.hr_post_level t16 on t1.post_level = t16.id 
left join hr_dai.hr_employment_category t17 on t1.employment_category = t17.id 
left join hr_dai.hr_entry_mode t18 on t1.entry_mode = t18.id 
left join hr_dai.hr_party_posts t19 on t2.party_job  = t19.id 
left join hr_dai.hr_education_degree t20 on t3."degree" = t20.id
left join (select * from politics_dwd.dwd_politics_party_member_detail_df 
where stat_month = (select max(stat_month) from  politics_dwd.dwd_politics_party_member_detail_df )
) t21 on t2.identity_id = t21.id_number --党建表
where t2.table_status = 1  and t1.extend_field_1  <> '吴闻力'
order by t15.order_sort asc
