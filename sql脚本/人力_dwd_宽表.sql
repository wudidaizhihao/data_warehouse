







-- -----------------------------------------------------------------------------
-- ---------------------------------建表sql_ods---------------------------------
-- -----------------------------------------------------------------------------








-- 集团内部任职经历

CREATE TABLE hr_dai.hr_work_experience_inner (
	id int8 NOT NULL, -- 序号
	employee_id varchar(50) NULL, -- 关联员工
	company_name varchar(255) NULL, -- 任职企业
	company_code varchar(64) NULL, -- 公司id，关联组织机构表
	company_level varchar(24) NULL, -- 企业组织级别 与机构树保持一致
	entry_mode int4 NULL, -- 入职方式   建表，关联表   社会招聘/院校招聘/内部调动/外部调动
	update_attribute int4 NULL, -- 修改属性
	employment_category int4 NULL, -- 用工类别   建表，关联表 合同工/劳务派遣/实习生/返聘
	position_start_date date NULL, -- 担任职位开始时间时间
	position_end_date date NULL, -- 担任职位结束时间
	job_status int4 NULL, -- 员工状态 1 在职 2 离职 3 退休 4 在岗 5 离岗
	startdt date NULL, -- 企业任职开始时间 
	enddt date NULL, -- 企业结束时间 当前未离职退休则为至今 或者 null
	alents int4 NULL, -- 是否为高级人才  0-否 1-是 
	post_type int4 NULL -- 岗位类型  见信息模板,-- 关联 岗位表
	post_level int4 NULL -- 层级 详情见信息模板,-- 关联 层级表
	position_attribute int4 NULL, -- 职位属性 1-高层管理 2-协管 3 其他
	second_department varchar(100) NULL, -- 二级部门
	three_department varchar(100) NULL, -- 三级部门
	four_department varchar(100) NULL, -- 四级部门
	five_department varchar(100) NULL, -- 五级部门
	job_name varchar(100) NULL, -- 职务名称
	job_type int4 NULL, -- 职委类型 1-任职 0-兼职
	job_flag int4 NULL, -- 标记，关联任职与兼职 job_flag相同表示同一分组，然后根据职委类型判断兼职任职
	part_time_num int4 NULL, -- 兼任企业数量 0-无兼任 1-一家兼任 2-两家兼任 3-三家兼任 4-四家兼任 5-五家兼任
	table_date timestamp NOT NULL, -- 修改和删除，都是重新添加一条记录
	table_status int4 NOT NULL, -- 状态 0 删除 1 可用 2 修改 3 彻底删除 4 5
	extend_field_1 varchar(255) NULL, -- 扩展字段1
	extend_field_2 varchar(255) NULL, -- 扩展字段2
	extend_field_3 int4 NULL, -- 扩展字段3
	extend_field_4 int4 NULL -- 扩展字段4
);
COMMENT ON TABLE hr_dai.hr_work_experience_inner IS '集团内部任职经历';



-- Column comments

COMMENT ON COLUMN hr_dai.hr_work_experience_inner.id IS '序号';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.employee_id IS '关联员工';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.company_name IS '任职企业';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.company_code IS '公司id，关联组织机构表';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.company_level IS '企业组织级别 与机构树保持一致';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.entry_mode IS '入职方式	  建表，关联表   社会招聘/院校招聘/内部调动/外部调动';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.update_attribute IS '修改属性';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.employment_category IS '用工类别	  建表，关联表 合同工/劳务派遣/实习生/返聘';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.position_start_date IS '担任职位开始时间时间';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.position_end_date IS '担任职位结束时间';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.job_status IS '员工状态	1 在职 2 离职 3 退休 4 在岗 5 离岗';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.startdt IS '企业任职开始时间 ';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.enddt IS '企业结束时间 当前未离职退休则为至今 或者 null';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.alents IS '是否为高级人才  0-否 1-是 ';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.post_type IS '岗位类型  见信息模板-- 关联 岗位表';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.post_level IS '层级 详情见信息模板-- 关联 层级表';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.position_attribute IS '职位属性 1-高层管理 2-协管 3 其他';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.second_department IS '二级部门';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.three_department IS '三级部门';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.four_department IS '四级部门';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.five_department IS '五级部门';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.job_name IS '职务名称';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.job_type IS '职委类型 1-任职 0-兼职';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.job_flag IS '标记，关联任职与兼职 job_flag相同表示同一分组，然后根据职委类型判断兼职任职';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.part_time_num IS '兼任企业数量 0-无兼任 1-一家兼任 2-两家兼任 3-三家兼任 4-四家兼任 5-五家兼任';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.table_date IS '修改和删除，都是重新添加一条记录';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.table_status IS '状态 0 删除 1 可用 2 修改 3 彻底删除 4 5';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.extend_field_1 IS '扩展字段1';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.extend_field_2 IS '扩展字段2';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.extend_field_3 IS '扩展字段3';
COMMENT ON COLUMN hr_dai.hr_work_experience_inner.extend_field_4 IS '扩展字段4';




















-- -----------------------------------------------------------------------------
-- ---------------------------------建表sql_dwd---------------------------------
-- -----------------------------------------------------------------------------






-- 员工职位经历明细事实表宽表

CREATE TABLE hr_dwd.dwd_hr_compo_position_change_wt_df (
	stat_month varchar(50) NULL, -- 统计年月
	stat_month_date date NULL, -- 统计年月_日期类型
	employee_id varchar(50) NULL, -- 员工编号
	emp_code varchar NULL, -- 公司人力编码
	company_code int4 NULL, -- 公司产权序号
	parent_id int8 NULL, -- 企业父编码
	abbreviation varchar NULL, -- 企业简称
	company_name varchar NULL, -- 公司名称
	emp_name varchar(255) NULL, -- 姓名
	gender text NULL, -- 性别
	education varchar NULL, -- 学历
	ethic varchar(24) NULL, -- 民族
	have_child varchar(24) NULL, -- 生育情况
	graduate_name varchar(200) NULL, -- 毕业学校
	graduate_type text NULL, -- 学校类型
	post_type_name varchar NULL, -- 岗位类型
	parent_post_type_name bpchar NULL, -- 岗位大类
	profession varchar NULL, -- 职业级别
	professional_title varchar NULL, -- 职称级别
	birth date NULL, -- 出生日期
	join_date date NULL, -- 入职日期
	work_date date NULL, -- 参加工作日期
	position_start_date date NULL, -- 担任职位开始时间
	position_end_date date NULL, -- 担任职位结束时间
	job_status varchar(50) NULL, -- 员工状态	1 在职 2 离职 3 退休 4 在岗 5 离岗
	age int4 NULL, -- 年龄
	join_age float8 NULL, -- 司龄
	work_age float8 NULL, -- 工龄
	age_grp text NULL, -- 年龄
	join_age_grp text NULL, -- 司龄段
	work_age_grp text NULL, -- 工龄段
	administrative_type varchar(24) NULL, -- 行政类型
	employment_category_name varchar(24) NULL, -- 用工类别
	country varchar NULL, -- 国家
	province varchar NULL, -- 省份
	city varchar NULL, -- 地市
	county varchar NULL, -- 区县
	longitude varchar NULL, -- 经度
	latitude varchar NULL, -- 维度
	email varchar(60) NULL, -- 邮箱
	image_path varchar(255) NULL, -- 头像
	politic_face text NULL, -- 政治面貌
	local_party_branch varchar(80) NULL, -- 所属党支部
	alents text NULL, -- 是否高级人员
	department varchar(100) NULL, -- 部门
	job_name varchar(100) NULL, -- 职务
	entry_mode_name varchar(24) NULL, -- 招聘方式
	identity_id varchar(50) NULL, -- 证件号
	phone varchar(30) NULL, -- 手机号
	order_sort int8 NULL, -- 排序号
	marital_status varchar NULL, -- 婚姻情况 0 未婚 1 已婚 2 离异
	etl_date date NULL DEFAULT CURRENT_DATE -- 更新时间
);
COMMENT ON TABLE hr_dwd.dwd_hr_compo_position_change_wt_df IS '员工职位经历明细事实表宽表';


-- Column comments

COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.stat_month IS '统计年月';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.stat_month_date IS '统计年月_日期类型';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.employee_id IS '员工编号';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.emp_code IS '公司人力编码';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.company_code IS '公司产权序号';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.parent_id IS '企业父编码';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.abbreviation IS '企业简称';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.company_name IS '公司名称';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.emp_name IS '姓名';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.gender IS '性别';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.education IS '学历';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.ethic IS '民族';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.have_child IS '生育情况';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.graduate_name IS '毕业学校';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.graduate_type IS '学校类型';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.post_type_name IS '岗位类型';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.parent_post_type_name IS '岗位大类';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.profession IS '职业级别';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.professional_title IS '职称级别';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.birth IS '出生日期';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.join_date IS '入职日期';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.work_date IS '参加工作日期';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.position_start_date IS '担任职位开始时间时间';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.position_end_date IS '担任职位结束时间';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.job_status IS '员工状态	1 在职 2 离职 3 退休 4 在岗 5 离岗';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.age IS '年龄';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.join_age IS '司龄';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.work_age IS '工龄';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.age_grp IS '年龄';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.join_age_grp IS '司龄段';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.work_age_grp IS '工龄段';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.administrative_type IS '行政类型';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.employment_category_name IS '用工类别';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.country IS '国家';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.province IS '省份';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.city IS '地市';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.county IS '区县';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.longitude IS '经度';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.latitude IS '维度';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.email IS '邮箱';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.image_path IS '头像';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.politic_face IS '政治面貌';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.local_party_branch IS '所属党支部';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.alents IS '是否高级人员';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.department IS '部门';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.job_name IS '职务';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.entry_mode_name IS '招聘方式';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.identity_id IS '证件号';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.phone IS '手机号';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.order_sort IS '排序号';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.marital_status IS '婚姻情况 0 未婚 1 已婚 2 离异';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_position_change_wt_df.etl_date IS '更新时间';















-- 员工状态变更明细事实表宽表-v1


-- CREATE TABLE hr_dwd.dwd_hr_compo_company_change_wt_df (
-- stat_year int4 NULL, -- 统计年份
-- stat_quarter date NULL, -- 统计季度
-- stat_type varchar(50) NULL, -- 统计类别
-- id int8 NULL, -- 序号
-- employee_id varchar(50) NULL, -- 关联员工
-- company_name varchar(255) NULL, -- 任职企业
-- company_code varchar(64) NULL, -- 公司id，关联组织机构表
-- company_level varchar(24) NULL, -- 企业组织级别 与机构树保持一致
-- entry_mode int4 NULL, -- 入职方式	  建表，关联表   社会招聘/院校招聘/内部调动/外部调动
-- update_attribute int4 NULL, -- 修改属性
-- employment_category int4 NULL, -- 用工类别	  建表，关联表 合同工/劳务派遣/实习生/返聘
-- position_start_date date NULL, -- 担任职位开始时间
-- position_end_date date NULL, -- 担任职位结束时间
-- job_status int4 NULL, -- 员工状态	1 在职 2 离职 3 退休 4 在岗 5 离岗
-- startdt date NULL, -- 企业任职开始时间 
-- enddt date NULL, -- 企业结束时间 当前未离职退休则为至今 或者 null
-- alents int4 NULL, -- 是否为高级人才  0-否 1-是 
-- post_type int4 NULL, -- 岗位类型  见信息模板-- 关联 岗位表
-- post_level int4 NULL, -- 层级 详情见信息模板-- 关联 层级表
-- position_attribute int4 NULL, -- 职位属性 1-高层管理 2-协管 3 其他
-- second_department varchar(100) NULL, -- 二级部门
-- three_department varchar(100) NULL, -- 三级部门
-- four_department varchar(100) NULL, -- 四级部门
-- five_department varchar(100) NULL, -- 五级部门
-- job_name varchar(100) NULL, -- 职务名称
-- job_type int4 NULL, -- 职委类型 1-任职 0-兼职
-- job_flag int4 NULL, -- 标记，关联任职与兼职 job_flag相同表示同一分组，然后根据职委类型判断兼职任职
-- part_time_num int4 NULL, -- 兼任企业数量 0-无兼任 1-一家兼任 2-两家兼任 3-三家兼任 4-四家兼任 5-五家兼任
-- table_date timestamp NULL, -- 修改和删除，都是重新添加一条记录
-- table_status int4 NULL, -- 状态 0 删除 1 可用 2 修改 3 彻底删除 4 5
-- extend_field_1 varchar(255) NULL, -- 扩展字段1
-- extend_field_2 varchar(255) NULL, -- 扩展字段2
-- extend_field_3 int4 NULL, -- 扩展字段3
-- extend_field_4 int4 NULL, -- 扩展字段4
-- position_rank int4 NULL, -- 员工职位经历排序
-- position_reverse_rank int4 NULL, -- 员工职位经历逆向排序
-- company_start_date date NULL, -- 企业任职开始时间_清洗后
-- company_end_date date NULL, -- 企业任职结束时间_清洗后
-- etl_date date NOT NULL DEFAULT CURRENT_DATE -- 调度更新时间
-- );
-- COMMENT ON TABLE hr_dwd.dwd_hr_compo_company_change_wt_df IS '员工状态变更明细事实表宽表';





-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.stat_year IS '统计年份';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.stat_quarter IS '统计季度';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.stat_type IS '统计类别';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.id IS '序号';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.employee_id IS '关联员工';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.company_name IS '任职企业';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.company_code IS '公司id，关联组织机构表';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.company_level IS '企业组织级别 与机构树保持一致';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.entry_mode IS '入职方式	  建表，关联表   社会招聘/院校招聘/内部调动/外部调动';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.update_attribute IS '修改属性';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.employment_category IS '用工类别	  建表，关联表 合同工/劳务派遣/实习生/返聘';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.position_start_date IS '担任职位开始时间';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.position_end_date IS '担任职位结束时间';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.job_status IS '员工状态	1 在职 2 离职 3 退休 4 在岗 5 离岗';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.startdt IS '企业任职开始时间 ';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.enddt IS '企业结束时间 当前未离职退休则为至今 或者 null';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.alents IS '是否为高级人才  0-否 1-是 ';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.post_type IS '岗位类型  见信息模板-- 关联 岗位表';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.post_level IS '层级 详情见信息模板-- 关联 层级表';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.position_attribute IS '职位属性 1-高层管理 2-协管 3 其他';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.second_department IS '二级部门';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.three_department IS '三级部门';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.four_department IS '四级部门';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.five_department IS '五级部门';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.job_name IS '职务名称';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.job_type IS '职委类型 1-任职 0-兼职';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.job_flag IS '标记，关联任职与兼职 job_flag相同表示同一分组，然后根据职委类型判断兼职任职';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.part_time_num IS '兼任企业数量 0-无兼任 1-一家兼任 2-两家兼任 3-三家兼任 4-四家兼任 5-五家兼任';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.table_date IS '修改和删除，都是重新添加一条记录';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.table_status IS '状态 0 删除 1 可用 2 修改 3 彻底删除 4 5';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.extend_field_1 IS '扩展字段1';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.extend_field_2 IS '扩展字段2';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.extend_field_3 IS '扩展字段3';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.extend_field_4 IS '扩展字段4';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.position_rank IS '员工职位经历排序';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.position_reverse_rank IS '员工职位经历逆向排序';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.company_start_date IS '企业任职开始时间_清洗后';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.company_end_date IS '企业任职结束时间_清洗后';
-- COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.etl_date IS '调度更新时间';
















-- 员工状态变更明细事实表宽表-v2

CREATE TABLE hr_dwd.dwd_hr_compo_company_change_wt_df (
	stat_year int4 NULL, -- 统计年份
	stat_quarter date NULL, -- 统计季度
	stat_type varchar(50) NULL, -- 统计类别
	employee_id varchar(50) NULL, -- 员工编号
	emp_code varchar NULL, -- 公司人力编码
	company_code int4 NULL, -- 公司产权序号
	parent_id int8 NULL, -- 企业父编码
	abbreviation varchar NULL, -- 企业简称
	company_name varchar NULL, -- 公司名称
	emp_name varchar(255) NULL, -- 姓名
	gender text NULL, -- 性别
	education varchar NULL, -- 学历
	ethic varchar(24) NULL, -- 民族
	have_child varchar(24) NULL, -- 生育情况
	graduate_name varchar(200) NULL, -- 毕业学校
	graduate_type text NULL, -- 学校类型
	post_type_name varchar NULL, -- 岗位类型
	parent_post_type_name bpchar NULL, -- 岗位大类
	profession varchar NULL, -- 职业级别
	professional_title varchar NULL, -- 职称级别
	birth date NULL, -- 出生日期
	join_date date NULL, -- 入职日期
	work_date date NULL, -- 参加工作日期
	position_start_date date NULL, -- 担任职位开始时间
	position_end_date date NULL, -- 担任职位结束时间
	job_status varchar(50) NULL, -- 员工状态	1 在职 2 离职 3 退休 4 在岗 5 离岗
	age int4 NULL, -- 年龄
	join_age float8 NULL, -- 司龄
	work_age float8 NULL, -- 工龄
	age_grp text NULL, -- 年龄
	join_age_grp text NULL, -- 司龄段
	work_age_grp text NULL, -- 工龄段
	administrative_type varchar(24) NULL, -- 行政类型
	employment_category_name varchar(24) NULL, -- 用工类别
	country varchar NULL, -- 国家
	province varchar NULL, -- 省份
	city varchar NULL, -- 地市
	county varchar NULL, -- 区县
	longitude varchar NULL, -- 经度
	latitude varchar NULL, -- 维度
	email varchar(60) NULL, -- 邮箱
	image_path varchar(255) NULL, -- 头像
	politic_face text NULL, -- 政治面貌
	local_party_branch varchar(80) NULL, -- 所属党支部
	alents text NULL, -- 是否高级人员
	department varchar(100) NULL, -- 部门
	job_name varchar(100) NULL, -- 职务
	entry_mode_name varchar(24) NULL, -- 招聘方式
	identity_id varchar(50) NULL, -- 证件号
	phone varchar(30) NULL, -- 手机号
	order_sort int8 NULL, -- 排序号
	marital_status varchar NULL, -- 婚姻情况 0 未婚 1 已婚 2 离异
	position_rank int4 NULL, -- 员工职位经历排序
	position_reverse_rank int4 NULL, -- 员工职位经历逆向排序
	company_start_date date NULL, -- 企业任职开始时间_清洗后
	company_end_date date NULL, -- 企业任职结束时间_清洗后
	etl_date date NOT NULL DEFAULT CURRENT_DATE -- 调度更新时间
);
COMMENT ON TABLE hr_dwd.dwd_hr_compo_company_change_wt_df IS '员工状态变更明细事实表宽表';



-- Column comments

COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.stat_year IS '统计年份';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.stat_quarter IS '统计季度';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.stat_type IS '统计类别';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.employee_id IS '员工编号';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.emp_code IS '公司人力编码';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.company_code IS '公司产权序号';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.parent_id IS '企业父编码';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.abbreviation IS '企业简称';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.company_name IS '公司名称';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.emp_name IS '姓名';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.gender IS '性别';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.education IS '学历';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.ethic IS '民族';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.have_child IS '生育情况';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.graduate_name IS '毕业学校';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.graduate_type IS '学校类型';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.post_type_name IS '岗位类型';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.parent_post_type_name IS '岗位大类';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.profession IS '职业级别';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.professional_title IS '职称级别';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.birth IS '出生日期';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.join_date IS '入职日期';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.work_date IS '参加工作日期';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.position_start_date IS '担任职位开始时间时间';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.position_end_date IS '担任职位结束时间';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.job_status IS '员工状态	1 在职 2 离职 3 退休 4 在岗 5 离岗';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.age IS '年龄';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.join_age IS '司龄';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.work_age IS '工龄';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.age_grp IS '年龄';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.join_age_grp IS '司龄段';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.work_age_grp IS '工龄段';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.administrative_type IS '行政类型';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.employment_category_name IS '用工类别';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.country IS '国家';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.province IS '省份';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.city IS '地市';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.county IS '区县';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.longitude IS '经度';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.latitude IS '维度';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.email IS '邮箱';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.image_path IS '头像';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.politic_face IS '政治面貌';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.local_party_branch IS '所属党支部';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.alents IS '是否高级人员';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.department IS '部门';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.job_name IS '职务';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.entry_mode_name IS '招聘方式';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.identity_id IS '证件号';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.phone IS '手机号';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.order_sort IS '排序号';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.marital_status IS '-- 婚姻情况 0 未婚 1 已婚 2 离异';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.position_rank IS '员工职位经历排序';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.position_reverse_rank IS '员工职位经历逆向排序';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.company_start_date IS '企业任职开始时间_清洗后';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.company_end_date IS '企业任职结束时间_清洗后';
COMMENT ON COLUMN hr_dwd.dwd_hr_compo_company_change_wt_df.etl_date IS '调度更新时间';















-- -----------------------------------------------------------------------------
-- ---------------------------------计算sql_dwd---------------------------------
-- -----------------------------------------------------------------------------





-- 员工职位经历明细事实表宽表

-- truncate table hr_dwd.dwd_hr_compo_position_change_wt_df;

insert into hr_dwd.dwd_hr_compo_position_change_wt_df

select 
	'2022-07' as stat_month,
	cast('2022-07-01' as date) as stat_month_date,
	t1.employee_id -- 员工编号
	-- 2022-05-23 BI端提出要求emp_code使用产权树的code,数据过滤需要用code处理
	,t15.emp_code as emp_code -- emp_code
	,t15.dept_id as company_code -- 公司编码
	,t15.parent_id -- 企业父编码
	,case when t15.abbreviation is null then '其他' else t15.abbreviation end as abbreviation -- 企业简称
	,case when t15.dept_name is null then '其他' else t15.dept_name end as company_name -- 公司名称
    ,t1.extend_field_1 as emp_name -- 姓名
	,case when t2.gender=0 then '女' when t2.gender=1 then '男' else '未' end as gender -- 性别
	,case when t4.name is null then '其他' else t4.name end as education -- 学历
	,t5.name as ethic -- 民族
	,t6.status as have_child -- 生育情况
	,case when t3.graduate_from='9999' then t3.graduate_from_other else t8.university_name end as graduate_name -- 毕业学校 t3.graduate_from 关联学校获取学校名称
	,case when t8.university_level='211' then '211学校' when t8.university_level='985' then '985学校' else '其他' end as graduate_type -- 学校类型 应该同上 毕业学校
	,case when t11.post_type_name is null then '其他' else t11.post_type_name end as post_type_name -- 岗位类型/职能分类
	,case when t11.parent_post_type_name is null then '其他' else t11.parent_post_type_name end as parent_post_type_name -- 岗位大类
	,case when t14.name is null then '其他' else t14.name end as profession -- 职业级别
	,case when t12.name is null then '其他' else t12.name end as professional_title -- 职称级别
	,t2.birth -- 出生日期
	,t2.join_date -- 入职日期
	,t2.work_date -- 参加工作日期
	,t1.position_start_date -- 担任职位开始时间
	,t1.position_end_date -- 担任职务结束时间
	,case when t1.job_status = 1 then '在职'
		when t1.job_status = 2 then '离职'
		when t1.job_status = 3 then '退休'
		when t1.job_status = 4 then '在岗'
		when t1.job_status = 5 then '离岗'
		else '其他' end as job_status
	,age -- 年龄
	,date_part('day',current_date::timestamp-t2.join_date::timestamp)/365 as join_age -- 司龄
	,date_part('day',current_date::timestamp-t2.work_date::timestamp)/365 as work_age -- 工龄
	,case when t2.age<=25 then '25岁以下' 
		when t2.age>25 and t2.age<=30 then '26-30岁' 
		when t2.age>30 and t2.age<=35 then '31-35岁'
		when t2.age>35 and t2.age<=40 then '36-40岁'
		when t2.age>40 and t2.age<=45 then '41-45岁'
		when t2.age>45 and t2.age<=50 then '46-50岁'
		when t2.age>50 then '50岁以上' else '其他' end as age_grp -- 年龄段
	,case when date_part('day',current_date::timestamp-t2.join_date::timestamp)/365<1 then '1年以下'
		when date_part('day',current_date::timestamp-t2.join_date::timestamp)/365>=1 and date_part('day',current_date::timestamp-t2.join_date::timestamp)/365<=3 then '1-3年'
		when date_part('day',current_date::timestamp-t2.join_date::timestamp)/365>3 and date_part('day',current_date::timestamp-t2.join_date::timestamp)/365<=5 then '4-5年'
		when date_part('day',current_date::timestamp-t2.join_date::timestamp)/365>5 and date_part('day',current_date::timestamp-t2.join_date::timestamp)/365<=10 then '6-10年'
		when date_part('day',current_date::timestamp-t2.join_date::timestamp)/365>10 and date_part('day',current_date::timestamp-t2.join_date::timestamp)/365<=15 then '11-15年'
		when date_part('day',current_date::timestamp-t2.join_date::timestamp)/365>15 and date_part('day',current_date::timestamp-t2.join_date::timestamp)/365<=20 then '16-20年'
		when date_part('day',current_date::timestamp-t2.join_date::timestamp)/365>20 then '20年以上' else '其他' end as join_age_grp -- 司龄段
	,case when date_part('day',current_date::timestamp-t2.work_date::timestamp)/365<1 then '1年以下'
		when date_part('day',current_date::timestamp-t2.work_date::timestamp)/365>=1 and date_part('day',current_date::timestamp-t2.work_date::timestamp)/365<=3 then '1-3年'
		when date_part('day',current_date::timestamp-t2.work_date::timestamp)/365>3 and date_part('day',current_date::timestamp-t2.work_date::timestamp)/365<=5 then '4-5年'
		when date_part('day',current_date::timestamp-t2.work_date::timestamp)/365>5 and date_part('day',current_date::timestamp-t2.work_date::timestamp)/365<=10 then '6-10年'
		when date_part('day',current_date::timestamp-t2.work_date::timestamp)/365>10 and date_part('day',current_date::timestamp-t2.work_date::timestamp)/365<=15 then '11-15年'
		when date_part('day',current_date::timestamp-t2.work_date::timestamp)/365>15 and date_part('day',current_date::timestamp-t2.work_date::timestamp)/365<=20 then '16-20年'
		when date_part('day',current_date::timestamp-t2.work_date::timestamp)/365>20 then '20年以上' else '其他' end as work_age_grp -- 工龄段
	,t16.name  administrative_type--行政类型 
	,t17.employment_category_name --用工类别
	,case when t15.country is null then '其他' else t15.country end as country -- 国家
	,case when t15.province is null then '其他' else t15.province end as province -- 省份
	,case when t15.city is null then '其他' else t15.city end as city -- 地市
	,case when t15.county is null then '其他' else t15.county end as county -- 区县
	,t15.longitude -- 经度
	,t15.dimension as latitude -- 维度
	,t2.email --邮箱
	,t2.image_path --头像
	,case when t2.politic_face = '1' then '党员' 
        when t2.politic_face = '2' then '预备党员'
		when t2.politic_face = '3' then '团员'
		when t2.politic_face = '4' then '其他党派'	
		when t2.politic_face = '5' then '群众' else '其他党派' end as politic_face  --政治面貌
	,t2.local_party_branch -- 所属党支部
	,case when t1.alents = '0' then '否' 
	    when t1.alents = '1' then '是'
		else '其他' end as alents --是否高级人员
	,t1.second_department as department -- 部门
	,t1.job_name --职务
	,t18.entry_mode_name -- 招聘方式
	,t2.identity_id --证件号
	,t2.phone --手机号
	,t15.order_sort -- 排序号
	,case when t2.marital_status = 0 then '未婚'
		when t2.marital_status = 1 then '已婚'
		when t2.marital_status = 2 then '离异'
		else '其他' end as marital_status -- 婚姻状态
from 
(
	select *,
			row_number() over (
				partition by employee_id, position_start_date, position_end_date order by table_date desc) as position_rank
	from hr_dai.hr_work_experience_inner
	where 1=1
	and table_status = 1 -- 筛选出可用数据
) t1
left join hr_dai.hr_employee t2 on t1.employee_id  = t2.employee_id
left join hr_dai.companies_match_dim t15 on  t15.company_code  = t1 .company_code --用公司编码作匹配
left join hr_dai.hr_education_experience_handle t3 on t1.employee_id=t3.employee_id and t3.stat_order =(select max(stat_order) from hr_dai.hr_education_experience_handle t20 where t1.employee_id = t20.employee_id ) --保留最高学历
left join hr_dai.hr_education_background t4 on t3.education=t4.id
left join hr_dai.hr_ethic t5 on t2.ethic=t5.ethic_id
left join hr_dai.hr_have_child t6 on t2.have_child=t6.id
left join hr_dai.hr_college_handle t8 on to_number(t3.second_graduate,'9999')=t8.coid
left join hr_dai.hr_post_type_handle t11 on t1.post_type=t11.id 
left join hr_dai.hr_current_status_handle t10 on t1.employee_id=t10.employee_id and 1=t10.stat_order
left join hr_dai.hr_contract_type t13 on t10.contract_type=t13.id 
left join hr_dai.hr_profession_skill_level t12 on t10.level_id=t12.id 
left join hr_dai.hr_licence_certification_level t14 on t10.license_level=t14.id
left join hr_dai.hr_post_level t16 on t1.post_level = t16.id 
left join hr_dai.hr_employment_category t17 on t1.employment_category = t17.id 
left join hr_dai.hr_entry_mode t18 on t1.entry_mode = t18.id 
where 1=1
and t1.position_rank = 1 -- 数据去重




















-- 员工状态变更明细事实表宽表-v1

-- insert into hr_dwd.dwd_hr_compo_company_change_wt_df

-- with ep as 
-- (
-- 	select *,
-- 			first_value(position_start_date) over (
-- 				partition by employee_id order by position_rank asc) as company_start_date,
-- 			first_value(position_end_date) over (
-- 				partition by employee_id order by position_rank desc) as company_end_date
-- 	from 
-- 	(
-- 		select *,
-- 				row_number() over (partition by employee_id order by position_start_date asc) as position_rank,
-- 				row_number() over (partition by employee_id order by position_start_date desc) as position_reverse_rank
-- 		from hr_dai.hr_work_experience_inner
-- 		where 1=1
-- 		and table_status=1
-- 		and position_start_date is not null
-- 	) tbl
-- 	where 1=1
-- 	order by employee_id asc,
-- 			position_rank asc
-- )




-- select *
-- from 
-- (
-- 	select extract('year' from ep.position_start_date) as stat_year,
-- 			cast(date_trunc('quarter', ep.position_start_date) as date) as stat_quarter,
-- 			'入职' as stat_type,
-- 			*
-- 	from ep
-- 	where 1=1
-- 	and to_char(ep.position_start_date, 'yyyy-MM-dd') between '2020-07-01' and '2022-06-30'
-- 	and position_rank = 1

-- 	union all

-- 	select extract('year' from ep.position_end_date) as stat_year,
-- 			cast(date_trunc('quarter', ep.position_end_date) as date) as stat_quarter,
-- 			'离职' as stat_type,
-- 			*
-- 	from ep
-- 	where 1=1
-- 	and to_char(ep.position_end_date, 'yyyy-MM-dd') between '2020-07-01' and '2022-06-30'
-- 	and position_reverse_rank = 1

-- 	union all

-- 	select dd.stat_year,
-- 			dd.stat_quarter,
-- 			'在职' as stat_type,
-- 			ep.*
-- 	from
-- 	(
-- 		select extract('year' from date) as stat_year,
-- 				cast(date_trunc('quarter', date) as date) as stat_quarter,
-- 				cast(date_trunc('month', date) as date) as stat_month,
-- 				row_number() over (partition by cast(date_trunc('quarter', date) as date)
-- 					order by cast(date_trunc('month', date) as date) desc) as month_rank,
-- 				min(date) as min_date,
-- 				max(date) as max_date
-- 		from dim.dim_date dd
-- 		where 1=1
-- 		and to_char(date, 'yyyy-MM-dd') between '2020-07-01' and '2022-06-30'
-- 		group by extract('year' from date),
-- 				cast(date_trunc('quarter', date) as date),
-- 				cast(date_trunc('month', date) as date)
-- 	) dd
-- 	cross join ep
-- 	where 1=1
-- 	and dd.month_rank = 1
-- 	and ep.position_start_date <= dd.max_date
-- 	and (ep.position_end_date is null or ep.position_end_date >= dd.min_date)
-- ) tbl
-- where 1=1
-- order by stat_year asc,
-- 		stat_quarter asc,
-- 		stat_type asc,
-- 		employee_id asc,
-- 		position_rank asc


















-- 员工状态变更明细事实表宽表-v2

-- truncate table hr_dwd.dwd_hr_compo_company_change_wt_df

insert into hr_dwd.dwd_hr_compo_company_change_wt_df

with ep as 
(
	select *,
			first_value(position_start_date) over (
				partition by employee_id order by position_rank asc) as company_start_date,
			first_value(position_end_date) over (
				partition by employee_id order by position_rank desc) as company_end_date
	from 
	(
		select *,
				row_number() over (partition by employee_id order by position_start_date asc, position_end_date asc) as position_rank,
				row_number() over (partition by employee_id order by position_start_date desc, position_end_date desc) as position_reverse_rank
		from hr_dwd.dwd_hr_compo_position_change_wt_df
		where 1=1
		and position_start_date is not null
	) tbl
	where 1=1
	order by employee_id asc,
			position_rank asc
)




SELECT stat_year,
       stat_quarter,
       stat_type,
       employee_id,
       emp_code,
       company_code,
       parent_id,
       abbreviation,
       company_name,
       emp_name,
       gender,
       education,
       ethic,
       have_child,
       graduate_name,
       graduate_type,
       post_type_name,
       parent_post_type_name,
       profession,
       professional_title,
       birth,
       join_date,
       work_date,
       position_start_date,
       position_end_date,
       job_status,
       age,
       join_age,
       work_age,
       age_grp,
       join_age_grp,
       work_age_grp,
       administrative_type,
       employment_category_name,
       country,
       province,
       city,
       county,
       longitude,
       latitude,
       email,
       image_path,
       politic_face,
       local_party_branch,
       alents,
       department,
       job_name,
       entry_mode_name,
       identity_id,
       phone,
       order_sort,
       marital_status,
       position_rank,
       position_reverse_rank,
       company_start_date,
       company_end_date
from 
(
	select extract('year' from ep.position_start_date) as stat_year,
			cast(date_trunc('quarter', ep.position_start_date) as date) as stat_quarter,
			'入职' as stat_type,
			*
	from ep
	where 1=1
	and to_char(ep.position_start_date, 'yyyy-MM-dd') between '2020-07-01' and '2022-06-30'
	and position_rank = 1

	union all

	select extract('year' from ep.position_end_date) as stat_year,
			cast(date_trunc('quarter', ep.position_end_date) as date) as stat_quarter,
			'离职' as stat_type,
			*
	from ep
	where 1=1
	and to_char(ep.position_end_date, 'yyyy-MM-dd') between '2020-07-01' and '2022-06-30'
	and position_reverse_rank = 1

	union all

	select dd.stat_year,
			dd.stat_quarter,
			'在职' as stat_type,
			ep.*
	from
	(
		select extract('year' from date) as stat_year,
				cast(date_trunc('quarter', date) as date) as stat_quarter,
				cast(date_trunc('month', date) as date) as stat_month,
				row_number() over (partition by cast(date_trunc('quarter', date) as date)
					order by cast(date_trunc('month', date) as date) desc) as month_rank,
				min(date) as min_date,
				max(date) as max_date
		from dim.dim_date dd
		where 1=1
		and to_char(date, 'yyyy-MM-dd') between '2020-07-01' and '2022-06-30'
		group by extract('year' from date),
				cast(date_trunc('quarter', date) as date),
				cast(date_trunc('month', date) as date)
	) dd
	cross join ep
	where 1=1
	and dd.month_rank = 1
	and ep.position_start_date <= dd.max_date
	and (ep.position_end_date is null or ep.position_end_date >= dd.min_date)
) tbl
where 1=1
order by stat_year asc,
		stat_quarter asc,
		stat_type asc,
		employee_id asc,
		position_rank asc




