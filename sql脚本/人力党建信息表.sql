-- hr_ads.ads_politics_hr_employee_info definition

-- Drop table

--DROP TABLE hr_ads.ads_politics_hr_employee_info;
CREATE TABLE hr_ads.ads_politics_hr_employee_info (
	id serial4 NOT NULL, -- 序号
	emp_name varchar NULL, -- 员工名称
	gender varchar(50) NULL, -- 性别
	ethic varchar(50) NULL, -- 民族
	birth date NULL, -- 出生日期
	work_date date NULL, -- 工作时间
	education varchar(50) NULL, -- 学历
	company_name varchar(50) NULL, -- 所在单位
	join_age date NULL, -- 任职时间
	etl_date date NOT NULL DEFAULT CURRENT_DATE
);
COMMENT ON TABLE hr_ads.ads_politics_hr_employee_info IS '人力_党员信息表';

-- Column comments

COMMENT ON COLUMN hr_ads.ads_politics_hr_employee_info.id IS '序号';
COMMENT ON COLUMN hr_ads.ads_politics_hr_employee_info.emp_name IS '员工名称';
COMMENT ON COLUMN hr_ads.ads_politics_hr_employee_info.gender IS '性别';
COMMENT ON COLUMN hr_ads.ads_politics_hr_employee_info.ethic IS '民族';
COMMENT ON COLUMN hr_ads.ads_politics_hr_employee_info.birth IS '出生日期';
COMMENT ON COLUMN hr_ads.ads_politics_hr_employee_info.work_date IS '工作时间';
COMMENT ON COLUMN hr_ads.ads_politics_hr_employee_info.education IS '学历';
COMMENT ON COLUMN hr_ads.ads_politics_hr_employee_info.company_name IS '所在单位';
COMMENT ON COLUMN hr_ads.ads_politics_hr_employee_info.join_age IS '任职时间';

-- Permissions

ALTER TABLE hr_ads.ads_politics_hr_employee_info OWNER TO prodba;
GRANT ALL ON TABLE hr_ads.ads_politics_hr_employee_info TO prodba;
	
delete from hr_ads.ads_politics_hr_employee_info

##人力党员信息表
INSERT INTO hr_ads.ads_politics_hr_employee_info
(emp_name, gender, ethic, birth, work_date, education, company_name, join_age,politic_face,dept_id ,parent_id ,code)
select 
 	 party_member_name --党员名称
	,sex --性别
	,nationality --民族
	,birthday --出生日期
	,start_work_date --工作时间
	,case when pm.education = '研究生班' then '研究生' 
	      when pm.education = '大学' then '大学' 
	      when pm.education = '研究生'then '研究生' 
	      else '其他' end as education -- 学历
	,pm.party_dept_name  -- 所在单位
	,pm.party_job_date -- 任职时间
	,'党员' politic_face --政治面貌
	,t2.company_code dept_id
	,t2.parent_id 
	,t2.emp_code  code 
from  politics_dwd.dwd_politics_party_member_detail_df  pm
left join dim.dim_politics_party_org_df po on pm.party_org_id = po.party_org_id 
left join hr_ads.ads_hr_employee_info t2 on pm.id_number = t2.identity_id
where 1=1 
and pm.stat_month  = (select max(stat_month) from politics_dwd.dwd_politics_party_member_detail_df )
and po.party_org_attribute = '党支部'
	union all
select 
 	emp_name
 	,gender
 	,ethic
 	,birth
 	,work_date
 	,education
 	,company_name
 	,join_date
	,politic_face
	,company_code dept_id
	,parent_id
	,emp_code code
from hr_ads.ads_hr_employee_info t1 
where 1=1
	and t1.politic_face !='党员'
	

	
	##根据公司名，更新数据
	update hr_ads.ads_politics_hr_employee_info 
	set dept_id  =  t2.dept_id 
	,parent_id  =t2.parent_id 
	,code = t2.code 
	from dim.dim_property_department t2
	where company_name = t2.dept_name 
	and ads_politics_hr_employee_info  .dept_id  is null
	
	update hr_ads.ads_politics_hr_employee_info 
	set dept_id  =  22
	,parent_id  =1
	,code = '001021'
	from dim.dim_property_department t2
	where 1=1
	and ads_politics_hr_employee_info  .company_name = '广州无线电集团广州广电城市服务集团股份有限公司'
	
	update hr_ads.ads_politics_hr_employee_info 
	set dept_id  =  62
	,parent_id  =13
	,code = '001012003'
	from dim.dim_property_department t2
	where 1=1
	and ads_politics_hr_employee_info  .company_name = '广州无线电集团海华公司'
	
	##将其他的设成无线电旗下的
	update hr_ads.ads_politics_hr_employee_info 
	set dept_id  = 1
	,code = '001'
	from dim.dim_property_department t2
	where 1=1
	and ads_politics_hr_employee_info  .company_name = '其他' or company_name  is null
	
	





