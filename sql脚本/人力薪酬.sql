select * from public.ads_hr_salary_module ahsm 

select * from public.ads_hr_artificial_cost ahac 



--查询
select count(distinct t1.dept_name) from public.ads_capital_invest_fin_profit t1
where t1.stat_month  = '2021-12'

select count(distinct t2.company_name ) from public.ads_hr_artificial_cost t2 
join  public.ads_hr_salary_module t3
on t2.company_name = t3.company_name  

select t1.dept_id 
,t1.dept_name 
,net_profit as 利润 
,t2.artificial_cost  as 人工成本
,(net_profit +artificial_cost) / artificial_cost 投资回报率 ,
t1.stat_month 
from ads_capital_invest_fin_profit t1 
join ads_hr_salary_module t2 on t1.dept_id  =t2.dept_id 
--join ads_hr_artificial_cost t3 on t1.dept_id  = t3.dept_id 
where  t1.stat_month  = '2021-12'  and t2.artificial_cost  != 0

--
select t1.dept_id 
,t1.company_name  
,t2.net_profit 
,t1.artificial_cost 
,t2.stat_month 
,t1.dept_id 
,T1.PARENT_ID
from ads_hr_salary_module t1 
join ads_capital_invest_fin_profit t2 on t1.dept_id  =t2.dept_id 
where  t2.stat_month  = '2021-12'  and t1.artificial_cost  != 0

--更新人力表dept_id 和parent_id ,先更新单体树
update ads_hr_artificial_cost  set dept_id = 
dim.dim_property_department .dept_id  
,parent_id  =  dim.dim_property_department .parent_id  
from dim.dim_property_department 
where ads_hr_artificial_cost.is_merge  = '否' 
and  dim.dim_property_department.is_head  = 0
and DIM.dim_property_department.dept_name  = ads_hr_artificial_cost.company_name 

--更新人力表dept_id 和parent_id ,更新合并树
update ads_hr_artificial_cost  set dept_id = 
dim.dim_property_department .dept_id  
,parent_id  =  dim.dim_property_department .parent_id  
from dim.dim_property_department 
where ads_hr_artificial_cost.is_merge  = '是' 
and  dim.dim_property_department.is_head  = 1
and DIM.dim_property_department.credit_code  = ads_hr_artificial_cost. credit_code

--删除匹配不上的数据
delete from ads_hr_artificial_cost where dept_id is null


--
--插入单位,及人工成本（薪酬总额）
delete from ads_hr_salary_module
insert into ads_hr_salary_module (company_name,artificial_cost,dept_id,parent_id)
select company_name,t1.actual_total ,dept_id,parent_id -- 实际工资
from ads_hr_artificial_cost t1

--更新投资回报率　(需排除分母为0得情况，若分母为0,即回报率为0)
update ads_hr_salary_module  set labor_cost = t3.labor_cost
from (
select t1.dept_id 
,t1.company_name  
,t2.net_profit as 利润
,t1.artificial_cost as 人力成本
,(net_profit +artificial_cost ) /artificial_cost  as labor_cost
,t2.stat_month 
,T1.PARENT_ID
from ads_hr_salary_module t1 
join ads_capital_invest_fin_profit t2 on t1.dept_id  =t2.dept_id 
where  t2.stat_month  = '2021-12'  and t1.artificial_cost  != 0 ) t3
where t3.dept_id  = ads_hr_salary_module.dept_id 


--算人均利润,取2021年12月，当月年累计利润，即是21年年利润
update ads_hr_salary_module set capita_profit = t4.capita_profit
from 
(
select t3.dept_id , t3.company_name  ,t2.credit_code  ,t2.total_profit ,t3.average_number 
,t2.report_type 
,total_profit /average_number as capita_profit
from capital.ods_fin_profit_sheet   t2 
 join public.ads_hr_artificial_cost t3 
on t2.credit_code  = t3.credit_code  
where 1=1
 and to_char(t2.data_time, 'YYYY-MM')  =  '2021-12'
and t3.average_number  !=0
and t2.report_type = t3.is_merge
and total_profit is not null
--and t2.company_name  like '%无线电%'
) t4
where t4.credit_code  = public .ads_hr_salary_module.credit_code  
and t4.report_type = public .ads_hr_salary_module.report_type  

--算人均资产利润回报率
update ads_hr_salary_module  set per_asset = t4.per_asset
from 
(
select
t2.dept_id 
,t3.capita_profit as 人均利润
,t1.total_asset  /t2.average_number  as 人均资产
,t3.capita_profit /(t1.total_asset  /t2.average_number)  as per_asset
,t1.total_asset
,t1.credit_code 
,t1.company_name 
,t1.report_type
,t2.average_number 
from capital.ods_fin_balance_sheet t1
join ads_hr_artificial_cost t2 on t1.credit_code = t2.credit_code 
and t1.report_type  =  t2.is_merge 
join ads_hr_salary_module t3 on t2.dept_id  = t3.dept_id 
where to_char (data_time,'YYYY-MM') = '2021-12'
and t2.average_number  != 0
and t3.capita_profit  is not null
and (t1.total_asset  /t2.average_number) != 0
) t4 where 
t4.dept_id  = ads_hr_salary_module.dept_id 


--算人力成本
update ads_hr_salary_module  set per_person  =  t2.per_person 
from 
(
select t1.dept_id 
,t1.company_name 
,t1.actual_total 
,t1.average_number 
,t1.actual_total /t1.average_number as per_person
from ads_hr_artificial_cost t1 where t1.average_number != 0) t2
where  ads_hr_salary_module.dept_id = t2.dept_id

--算工资总额，
update ads_hr_salary_module  set total_wages = t2.total_wages
from
(
select t1.dept_id 
,t1.company_name 
,t1.total_amount as  total_wages
from ads_hr_artificial_cost t1 where dept_id is not null ) t2
where ads_hr_salary_module.dept_id  = t2.dept_id 

--算人均资产

----
select dept_id ,company_name 
,total_wages as 工资总额
, artificial_cost as 人工成本
,labor_cost  as 人工成本回报率
,per_person  as 人均人力成本 
from ads_hr_salary_module ahsm 


### 新逻辑:
update hr.ads_hr_tenure_salary_module set report_type = '合并' 
where report_type = '是'

##从人工成本表插入数据至薪酬表（人工成本只有21年数据，但薪酬表又19-21年，因为后面直接从excel入库到薪酬表了）
insert into hr.ads_hr_tenure_salary_module
(total_wages, artificial_cost,annual, dept_id, parent_id, company_name, credit_code, report_type)
select total_amount,actual_total,annual,dept_id, parent_id, company_name,credit_code,is_merge
from hr.ads_hr_artificial_cost


###更新一下dept_id 等信息
update  hr.ads_hr_tenure_salary_module 
set
 dept_id = t3 .dept_id 
,parent_id  = t3.parent_id 
,report_type  = t3.report_type 
from (
select * from (
select  t1.report_type ,t1.dept_id ,t1.credit_code ,t1.parent_id  ,code ,
row_number () OVER(partition by credit_code order by report_type asc ) num
from dim.dim_property_department  t1 ) t2 where t2.num  = 1 ) t3
where hr.ads_hr_tenure_salary_module  .credit_code  = t3.credit_code 
and hr.ads_hr_tenure_salary_module .credit_code  in(
select credit_code from hr.ads_hr_tenure_salary_module  t1 where   report_type is null
 )  and  hr.ads_hr_tenure_salary_module .report_type is null
 
 
  ##增加公司营收收入
 update  hr.ads_hr_tenure_salary_module 
 set in_operating_income =  capital.ads_capital_fin_wide_analysis.in_operating_income
 from  capital.ads_capital_fin_wide_analysis
 where  hr.ads_hr_tenure_salary_module .credit_code  =  capital.ads_capital_fin_wide_analysis.credit_code 
 and hr.ads_hr_tenure_salary_module .report_type  =  capital.ads_capital_fin_wide_analysis.report_type 
 and  TO_DATE (concat_ws(hr.ads_hr_tenure_salary_module .annual,'','-12-01'),'YYYY-MM-DD') =  capital.ads_capital_fin_wide_analysis.data_time  
 
 ##更新人数
  update  hr.ads_hr_tenure_salary_module 
 set average_number = hr.ads_hr_artificial_cost.average_number 
 from ads_hr_artificial_cost
 where hr.ads_hr_tenure_salary_module .credit_code  = hr.ads_hr_artificial_cost.credit_code 
 and hr.ads_hr_tenure_salary_module.report_type  = hr.ads_hr_artificial_cost.is_merge 
 
 
## 

#####整体思路：
1.从人工成本表 加载19年至21年数据
2.员工薪酬总额作为：计划工资、工资总额作为：实际工资
3.薪酬总额同时为人工成本
4.人均利润取财务表(ads_capital_fin_wide_analysis)
5.人均营收取新的总表(ads_capital_fin_wide_analysis,本年上月累计营业总收入)


