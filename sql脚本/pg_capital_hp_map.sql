CREATE TABLE capital_ads.ads_asset_map (
	stat_year date NULL, -- 统计日期
	province varchar(10) NULL, -- 所属省份
	city varchar(10) NULL, -- 所属城市
	county varchar(10) NULL, -- 所属区县
	index_name varchar(20) null, -- 指标名称
	index_val numeric(20, 2) null, -- 指标值
	upd_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP -- 更新日期
);
COMMENT ON TABLE capital_ads.ads_asset_map IS '资产地图';

-- Column comments

COMMENT ON COLUMN capital_ads.ads_asset_map.stat_year IS '统计日期';
COMMENT ON COLUMN capital_ads.ads_asset_map.province IS '所属省份';
COMMENT ON COLUMN capital_ads.ads_asset_map.city IS '所属城市';
COMMENT ON COLUMN capital_ads.ads_asset_map.county IS '所属区县';
COMMENT ON COLUMN capital_ads.ads_asset_map.index_name IS '指标名称';
COMMENT ON COLUMN capital_ads.ads_asset_map.index_val IS '指标值';
COMMENT ON COLUMN capital_ads.ads_asset_map.upd_date IS '更新日期';






GRANT SELECT ON capital.ads_asset_map TO grgdba;

truncate table capital.ads_asset_map;
truncate table capital_ads.ads_asset_map;

select * from capital_ads.ads_asset_map t where t.county like '%县%'


insert into capital_ads.ads_asset_map
select current_date as stat_date,
		case when tmp.province='广西壮族自治区' then '广西'
			when tmp.province='内蒙古自治区' then '内蒙古' else tmp.province end province,
		tmp.city,
		tmp.county,
		tmp.index_name,
		tmp.index_val 
from (select replace(province, '省', '') province,replace(city, '市', '') city, replace(county, '区', '') county,
		'企业数量' as index_name,
	    count(distinct company_code) as index_val
	from hr_ads.ads_hr_employee_info t1
	where province <> ''
	group by province, city, county
	
	union all
	select replace(province, '省', '') province,replace(city, '市', '') city, replace(county, '区', '') county,
			'员工数量' as index_name,
			count(1) as index_val
	from hr_ads.ads_hr_employee_info ahei  
	where province <>''
	group by country ,city ,province ,county
	
	union all 
select replace(t.province, '省', '') province,replace(t.city, '市', '') city, replace(t.county, '区', '') county, unnest(string_to_array(stat_type,',')) index_name,
		cast( unnest(string_to_array(concat_ws(',', t.total_operating_income,t.total_profit,t.total_asset), ',')) as numeric) index_val
	from(select province, -- 财务指标
			 city, 
			 county,
			 '营业收入,利润总额,资产总额' stat_type,
			 sum(total_operating_income) as total_operating_income,
			 sum(total_profit) as total_profit,
			 sum(total_asset) as total_asset
		from (select t3.province_shortname province,
				 t3.city_shortname city,
				 t3.county_shortname county,
				 t1.credit_code,
				 t1.total_operating_income,
				 t1.total_profit,
				 t2.total_asset
				 -- count(1)
			from (select * from capital.ods_fin_profit_sheet_xls where to_char(data_time, 'yyyy-mm')='2021-12' and report_type='单体' and credit_code<>'')t1
			left join (select * from capital.ods_fin_balance_sheet_xls where to_char(data_time, 'yyyy-mm')='2021-12' and report_type='单体' and credit_code<>'')t2 
			on t1.credit_code=t2.credit_code and to_char(t1.data_time, 'yyyy-mm') = to_char(t2.data_time, 'yyyy-mm') and t1.report_type=t2.report_type 
			left join dim.dim_finance_tree_region_change t3 on t1.credit_code=t3.credit_code and t1.report_type=t3.report_type) tmp
		group by province, city, county) t
		
		union all



select replace(t.province, '省', '') province
      ,replace (replace (replace(t.city, '市', ''),'兴安盟乌兰浩特','兴安盟'),'巴州库尔勒','巴州' )city
      , '' county, unnest(string_to_array(stat_type,',')) index_name,
		cast( unnest(string_to_array(concat_ws(',', t.invest_project_cnt,t.invest_project_amount), ',')) as numeric) index_val
	from(select '投资项目数量,投资项目金额' as stat_type, 
			replace(project_province, '市', '') as province, 
			project_city as city, 
			count(distinct project_name) as invest_project_cnt,
			sum(actual_invest_amount) as invest_project_amount
		from capital_ads.ads_capital_invest_project_invest_fin_detail
		where 1=1 and project_province <>'' 
		and dept_id = 1 -- 无线电集团
		group by project_province,project_city) t
		
		union all


select t.province, t.city, t.county, unnest(string_to_array(stat_type,',')) index_name,
		cast( unnest(string_to_array(concat_ws(',', t.property_cnt,t.property_area), ',')) as numeric) index_val
	from (select '物业宗数,物业面积' stat_type,-- 物业数量、实际面积
			province,
			city,
			county,
			count(1) as property_cnt, 
			sum(area_actual) as property_area  -- 物业实际面积	
	from (
		select id, bloc_name, control_unit, property_name, province,
			case when province ='北京' and city='市辖区' then '北京' 
				when province ='上海' and city='市辖区' then '上海' 
				when province ='深圳' and city='市辖区' then '深圳' 
				when province ='重庆' and city='市辖区' then '重庆' 
				else city end city,
			county, street, address, confirmed, area_load,
			area_actual,utility_load, utility_actual, mortgage,
			litigation, wholly, property_category, remark
		from capital.ods_property_basic_total_month
	) tbl
	group by province, city, county) t
	union all
	------------------------- 土地数量、占地面积
	select t.province, t.city, t.county, unnest(string_to_array(stat_type,',')) index_name,
		cast( unnest(string_to_array(concat_ws(',', t.land_cnt,t.land_area), ',')) as numeric) index_val
	from(select '土地宗数,土地面积' as stat_type, -- 土地数量、占地面积
			province, 
			city,
			county, 
			count(1) as land_cnt,  -- 土地数量
			sum(covers_area) as land_area --占地面积
	from (
		select serial_number, group_name, enterprise_name, site_name,
		  equity_ratio, other_shareholders, shareholders_shares,province,
		  case when province ='北京' and city='市辖区' then '北京' 
		    when province ='上海' and city='市辖区' then '上海' 
		    when province ='深圳' and city='市辖区' then '深圳' 
		    when province ='重庆' and city='市辖区' then '重庆' 
		    else city end city, 
		  county, street, detailed_address, four_range,
		  covers_area, construction_area, confirm_area, structure_area, is_confirmed
		from capital.ods_property_land_assets_total_year
	) tbl
	group by province, city, county) t
	union all
	------------------------- 产业园数量、占地面积
	select t.province, t.city, t.county, unnest(string_to_array(stat_type,',')) index_name,
		cast( unnest(string_to_array(concat_ws(',', t.industrial_park_cnt,t.industrial_park_area), ',')) as numeric) index_val
	from(select '产业园个数,产业园面积' as stat_type, -- 产业园数量、占地面积
			province,
			city,
		    county, 
			count(1) as industrial_park_cnt,  -- 产业园数量
			sum(covers_area) as industrial_park_area --占地面积
	from (select id, park_name, is_keypark, province,
	  case when province ='北京' and city='市辖区' then '北京' 
	    when province ='上海' and city='市辖区' then '上海' 
	    when province ='深圳' and city='市辖区' then '深圳' 
	    when province ='重庆' and city='市辖区' then '重庆' 
	    else city end city, 
	  county, enterprise_name, enterprises_nature, address,
	  land_nature, construction_schedule, building_number,
	  covers_area, gross_area, above_area
	 from capital.ods_property_group_park_information_total_year) tbl
	group by province ,city,county) t
	) tmp
	
GRANT SELECT ON riskcontrol.ads_risk_audit_department_data TO grgdba;

--跑完上面sql后，直接下列sql，修改省份部分数据
update capital_ads.ads_asset_map 
set province = '北京' where province = '北京市'
update capital_ads.ads_asset_map 
set province = '上海' where province = '上海市'
update capital_ads.ads_asset_map 
set province = '香港' where province = '香港特别行政区	'


----------------------------------------行转列，给bi使用sql
select t1.stat_year as 统计年月
,t1.province as 省份
,t1.city as 城市
,t1.county  as 区县
,case when index_name  = '企业数量' then index_val  else 0 end as 企业数量
,case when index_name  = '物业宗数' then index_val  else 0 end as 物业宗数
,case when index_name  = '产业园面积' then index_val  else 0 end as 产业园面积
,case when index_name  = '产业园个数' then index_val  else 0 end as 产业园个数
,case when index_name  = '土地面积' then index_val  else 0 end as 土地面积

,case when index_name  = '物业面积' then index_val  else 0 end as 物业面积
,case when index_name  = '资产总额' then index_val  else 0 end as 资产总额
,case when index_name  = '投资项目金额' then index_val  else 0 end as 投资项目金额
,case when index_name  = '员工数量' then index_val  else 0 end as 员工数量
,case when index_name  = '投资项目数量' then index_val  else 0 end as 投资项目数量

,case when index_name  = '利润总额' then index_val  else 0 end as 利润总额
,case when index_name  = '营业收入' then index_val  else 0 end as 营业收入
,case when index_name  = '土地宗数' then index_val  else 0 end as 土地宗数
from  capital_ads.ads_asset_map  t1






-----------------------------------------以下为行
	
-- 列转行
select t.name1, unnest(t.t1) 
from (with tmp_table as (
	select 'a' name1,'aa'::varchar as t union all
	select 'a' name1,'bb'::varchar as t union all
	select 'a' name1,'cc'::varchar as t
	)
	select name1,array_agg(t) t1,string_agg(t,',') t2
	from tmp_table
	group by name1) t

-- 财务列转行
select t.province, t.city, t.county, unnest(string_to_array(stat_type,',')) index_name,
	unnest(string_to_array(concat_ws(',', t.total_operating_income,t.total_profit,t.total_asset), ',')) index_val
from(select province, -- 财务指标
		 city, 
		 county,
		 '营业总收入,利润总额,资产总额' stat_type,
		 sum(total_operating_income) as total_operating_income,
		 sum(total_profit) as total_profit,
		 sum(total_asset) as total_asset
	from (select t3.province,
			 t3.city,
			 t3.county,
			 t1.credit_code,
			 t1.total_operating_income,
			 t1.total_profit,
			 t2.total_asset
			 -- count(1)
		from (select * from capital.ods_fin_profit_sheet_xls where to_char(data_time, 'yyyy-mm')='2021-12' and report_type='单体' and credit_code<>'')t1
		left join (select * from capital.ods_fin_balance_sheet_xls where to_char(data_time, 'yyyy-mm')='2021-12' and report_type='单体' and credit_code<>'')t2 
		on t1.credit_code=t2.credit_code and to_char(t1.data_time, 'yyyy-mm') = to_char(t2.data_time, 'yyyy-mm') and t1.report_type=t2.report_type 
		left join dim.dim_finance_tree t3 on t1.credit_code=t3.credit_code and t1.report_type=t3.report_type) tmp
	group by province, city, county) t
order by t.province, t.city, t.county

-- 投资项目和金额 列转行
select t.province, t.city, unnest(string_to_array(stat_type,',')) index_name,
	unnest(string_to_array(concat_ws(',', t.invest_project_cnt,t.invest_project_amount), ',')) index_val
from(select '投资项目数量,投资项目金额' as stat_type, 
		project_province as province, 
		project_city as city, 
		count(distinct project_name) as invest_project_cnt,
		sum(actual_invest_amount) as invest_project_amount
	from capital_ads.ads_capital_invest_project_invest_fin_detail
	where 1=1 and project_province <>'' 
	and dept_id = 1 -- 无线电集团
	group by project_province,project_city) t
order by t.province, t.city

-- 物业
select t.province, t.city, t.county, unnest(string_to_array(stat_type,',')) index_name,
	unnest(string_to_array(concat_ws(',', t.property_cnt,t.property_area), ',')) index_val 
from (select '物业数量,物业实际面积' stat_type,-- 物业数量、面积
		province,
		city,
		county,
		count(1) as property_cnt, 
		sum(area_actual) as property_area  -- 物业实际面积	
from (
	select id, bloc_name, control_unit, property_name, province,
		case when province ='北京' and city='市辖区' then '北京' 
			when province ='上海' and city='市辖区' then '上海' 
			when province ='深圳' and city='市辖区' then '深圳' 
			when province ='重庆' and city='市辖区' then '重庆' 
			else city end city,
		county, street, address, confirmed, area_load,
		area_actual,utility_load, utility_actual, mortgage,
		litigation, wholly, property_category, remark
	from capital.ods_property_basic_total_month
) tbl
group by province, city, county) t



------------------------------以下是旧表结构废弃-------------------------------------------
CREATE TABLE capital_ads.ads_asset_map (
	stat_year varchar(10) NULL, -- 统计年度
	stat_type varchar(10) NULL, -- 统计类别
	province varchar(10) NULL, -- 所属省份
	city varchar(10) NULL, -- 所属城市
	county varchar(10) NULL, -- 所属区县
	company_cnt int4 NULL, -- 企业数量
	employee_cnt int4 NULL, -- 员工数量
	total_operating_income numeric(20, 2) NULL, -- 营业总收入
	total_profit numeric(20, 2) NULL, -- 利润总额
	total_asset numeric(20, 2) NULL, -- 资产总计
	invest_project_cnt int4 NULL, -- 投资项目数量
	invest_project_amount numeric(20, 2) NULL, -- 投资项目金额
	property_cnt int4 NULL, -- 物业宗数
	property_area numeric(20, 2) NULL, -- 物业面积
	land_cnt int4 NULL, -- 土地宗数
	land_area numeric(20, 2) NULL, -- 土地面积
	industrial_park_cnt int4 NULL, -- 产业园数量
	industrial_park_area numeric(20, 2) NULL -- 产业园面积
);
COMMENT ON TABLE capital_ads.ads_asset_map IS '资产地图';

-- Column comments

COMMENT ON COLUMN capital_ads.ads_asset_map.stat_year IS '统计年度';
COMMENT ON COLUMN capital_ads.ads_asset_map.stat_type IS '统计类别';
COMMENT ON COLUMN capital_ads.ads_asset_map.province IS '所属省份';
COMMENT ON COLUMN capital_ads.ads_asset_map.city IS '所属城市';
COMMENT ON COLUMN capital_ads.ads_asset_map.county IS '所属区县';
COMMENT ON COLUMN capital_ads.ads_asset_map.company_cnt IS '企业数量';
COMMENT ON COLUMN capital_ads.ads_asset_map.employee_cnt IS '员工数量';
COMMENT ON COLUMN capital_ads.ads_asset_map.total_operating_income IS '营业总收入';
COMMENT ON COLUMN capital_ads.ads_asset_map.total_profit IS '利润总额';
COMMENT ON COLUMN capital_ads.ads_asset_map.total_asset IS '资产总计';
COMMENT ON COLUMN capital_ads.ads_asset_map.invest_project_cnt IS '投资项目数量';
COMMENT ON COLUMN capital_ads.ads_asset_map.invest_project_amount IS '投资项目金额';
COMMENT ON COLUMN capital_ads.ads_asset_map.property_cnt IS '物业宗数';
COMMENT ON COLUMN capital_ads.ads_asset_map.property_area IS '物业面积';
COMMENT ON COLUMN capital_ads.ads_asset_map.land_cnt IS '土地宗数';
COMMENT ON COLUMN capital_ads.ads_asset_map.land_area IS '土地面积';
COMMENT ON COLUMN capital_ads.ads_asset_map.industrial_park_cnt IS '产业园数量';
COMMENT ON COLUMN capital_ads.ads_asset_map.industrial_park_area IS '产业园面积';


truncate table capital_ads.ads_asset_map;

insert into capital_ads.ads_asset_map
select '2022' as stat_year,
		*
from 
( 
select 
	'企业数' as stat_type,
	province, -- 企业数量
	city,
	county,
    count(distinct credit_code) as company_cnt,
    0 as employee_cnt,
	0 as total_operating_income,
	0 as total_profit,
	0 as total_asset,
	0 as invest_project_cnt,
	0 as invest_project_amount,
	0 as property_cnt,
	0 as property_area,
	0 as land_cnt,
	0 as land_area,
	0 as industrial_park_cnt,
	0 as industrial_park_area
from dim.dim_property_department t1
where province <> ''
group by province, city, county

union all

select '人力' as stat_type,
		province,
		city,
		county,
		0 as company_cnt,
		count(1) as employee_cnt,
		0 as total_operating_income,
		0 as total_profit,
		0 as total_asset,
		0 as invest_project_cnt,
		0 as invest_project_amount,
		0 as property_cnt,
		0 as property_area,
		0 as land_cnt,
		0 as land_area,
		0 as industrial_park_cnt,
		0 as industrial_park_area
from hr_dai.ads_hr_talent_map 
group by country ,city ,province ,county


union all

select '财务' as stat_type,
	 province, -- 财务指标
	 city, 
	 county,
	 0 as company_cnt,
	 0 as employee_cnt,
	 sum(total_operating_income) as total_operating_income,
	 sum(total_profit) as total_profit,
	 sum(total_asset) as total_asset,
	 0 as invest_project_cnt,
	0 as invest_project_amount,
	0 as property_cnt,
	0 as property_area,
	0 as land_cnt,
	0 as land_area,
	0 as industrial_park_cnt,
	0 as industrial_park_area
from
(
	select 
		 t3.province,
		 t3.city,
		 t3.county,
		 t1.credit_code,
		 t1.total_operating_income,
		 t1.total_profit,
		 t2.total_asset
		 -- count(1)
	from (select * from capital.ods_fin_profit_sheet_xls where to_char(data_time, 'yyyy-mm')='2021-12' and report_type='单体' and credit_code<>'')t1
	left join (select * from capital.ods_fin_balance_sheet_xls where to_char(data_time, 'yyyy-mm')='2021-12' and report_type='单体' and credit_code<>'')t2 
	on t1.credit_code=t2.credit_code and to_char(t1.data_time, 'yyyy-mm') = to_char(t2.data_time, 'yyyy-mm') and t1.report_type=t2.report_type 
	left join dim.dim_finance_tree t3 on t1.credit_code=t3.credit_code and t1.report_type=t3.report_type
) tbl
group by province, city, county

union all

select '投资' as stat_type, 
		project_province as province, 
		project_city as city, 
		null as county, 
		0 as company_cnt,
	    0 as employee_cnt,
		0 as total_operating_income,
		0 as total_profit,
		0 as total_asset,
		count(distinct project_name) as invest_project_cnt,
		sum(actual_invest_amount) as invest_project_amount,
		0 as property_cnt,
		0 as property_area,
		0 as land_cnt,
		0 as land_area,
		0 as industrial_park_cnt,
		0 as industrial_park_area
from capital_ads.ads_capital_invest_project_invest_fin_detail
where 1=1
and dept_id = 1 -- 无线电集团
group by project_province,
		project_city

union all

select '物业' stat_type, -- 物业数量、面积
		province,
		city,
		county,
		0 as company_cnt,
	    0 as employee_cnt,
		0 as total_operating_income,
		0 as total_profit,
		0 as total_asset,
		0 as invest_project_cnt,
		0 as invest_project_amount,
		count(1) as property_cnt, 
		sum(area_actual) as property_area,  -- 物业实际面积
		0 as land_cnt,
		0 as land_area,
		0 as industrial_park_cnt,
		0 as industrial_park_area
from 
(
	select id, bloc_name, control_unit, property_name, province,
		case when province ='北京' and city='市辖区' then '北京' 
			when province ='上海' and city='市辖区' then '上海' 
			when province ='深圳' and city='市辖区' then '深圳' 
			when province ='重庆' and city='市辖区' then '重庆' 
			else city end city,
		county, street, address, confirmed, area_load,
		area_actual,utility_load, utility_actual, mortgage,
		litigation, wholly, property_category, remark
	from capital.ods_property_basic_total_month
) tbl
group by province, city, county

union all

select '土地' as stat_type, -- 土地数量、面积
		province, 
		city,
		county, 
		0 as company_cnt,
		0 as employee_cnt,
		0 as total_operating_income,
		0 as total_profit,
		0 as total_asset,
		0 as invest_project_cnt,
		0 as invest_project_amount,
		0 as property_cnt, 
		0 as property_area,  -- 物业实际面积
		count(1) as land_cnt,  -- 土地数量
		sum(covers_area) as land_area, --占地面积
		0 as industrial_park_cnt,
		0 as industrial_park_area
from 
(
	select serial_number, group_name, enterprise_name, site_name,
	  equity_ratio, other_shareholders, shareholders_shares,province,
	  case when province ='北京' and city='市辖区' then '北京' 
	    when province ='上海' and city='市辖区' then '上海' 
	    when province ='深圳' and city='市辖区' then '深圳' 
	    when province ='重庆' and city='市辖区' then '重庆' 
	    else city end city, 
	  county, street, detailed_address, four_range,
	  covers_area, construction_area, confirm_area, structure_area, is_confirmed
	from capital.ods_property_land_assets_total_year
) tbl
group by province, city, county
union all

select '产业园' as stat_type, -- 产业园数量、面积
		province,
		city,
		area as county, 
		0 as company_cnt,
		0 as employee_cnt,
		0 as total_operating_income,
		0 as total_profit,
		0 as total_asset,
		0 as invest_project_cnt,
		0 as invest_project_amount,
		0 as property_cnt, 
		0 as property_area,  -- 物业实际面积
		0 as land_cnt,  -- 土地数量
		0 as land_area, --占地面积
		count(1) as industrial_park_cnt,  -- 产业园数量
		sum(covers_area) as industrial_park_area --占地面积
from 
(select id, park_name, is_keypark, province,
  case when province ='北京' and city='市辖区' then '北京' 
    when province ='上海' and city='市辖区' then '上海' 
    when province ='深圳' and city='市辖区' then '深圳' 
    when province ='重庆' and city='市辖区' then '重庆' 
    else city end city, 
  area, enterprise_name, enterprises_nature, address,
  land_nature, construction_schedule, building_number,
  covers_area, gross_area, above_area
 from capital.ods_property_group_park_information_total_year
) tbl
group by province ,city,area

) tbl

