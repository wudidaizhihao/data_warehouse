营业收入
利润总额
净利润
资产总额
净资产
期末员工人数
科研投入
利润总额
insert into ads_analysis_industry_total_month_backup 
select 
t1.company_name,
'2022年Q1' annual,
t1.company_name enterprise_name, 
'毛利率' "type"
--,t1.data_time 
--,T1.total_profit num --利润总额
--,t1.net_profit  num --净利润
--,t1.in_operating_income  num --营业收入
--,t2.total_asset num--资产合计 total_profit
,t3.sale_gross_profit_ratio num--毛利率
--,t1 .report_type  
from capital.ods_fin_profit_sheet  t1 
join capital .ods_fin_balance_sheet t2 
on t1.company_name  = t2.company_name 
join capital_ads.ads_capital_fin_wide_analysis t3
on t2.company_name  = t3.company_name 
where  (t1.company_name 
like '%广州广电运通金融电子股份有限公司%' or t1.company_name  like '%广州海格通信集团股份有限公司%')
and to_char(t1.data_time,'YYYY-MM') = '2022-02'  and t1.report_type  = '合并'
and to_char(t2.data_time,'YYYY-MM') = '2022-02'  and t2.report_type  = '合并'
and to_char(t3.data_time,'YYYY-MM') = '2022-02'  and t3.report_type  = '合并'




--
select sale_gross_profit_ratio, curr_sale_gross_profit_ratio from capital_ads.ads_capital_fin_wide_analysis

	sale_gross_profit_ratio numeric(20, 2) NULL, -- 本年本月累计销售毛利率
	sale_net_profit_ratio numeric(20, 2) NULL, -- 本年本月累计销售净利率
	curr_sale_gross_profit_ratio numeric(20, 2) NULL, -- 本年当月销售毛利率

1.total_profit --利润总额
2.净利润  --net_profit
3其中营业收入  --in_operating_income