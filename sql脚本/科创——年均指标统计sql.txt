--研发投入占营收比及年均研发费用
select  avg(in_operating_income) as 总营收  ,sum(investment_money)*10000 as 总研发投入
,sum(investment_money) /count(distinct annual) as 年均研发收入_万元
, round((sum(investment_money)*10000 /avg(in_operating_income)) *100,2) as 研发占营收占比
from (
select in_operating_income ,data_time ,t2.investment_money ,t2.annual 
from capital.ads_capital_fin_wide_analysis t1 
,tech.ads_tech_technology_total_year t2
where  t1.parent_id =  0  and t1.data_time  = '2021-12-01' ) t3

--科技奖励年均
select sum(奖励金额)/count(distinct 年份) as 年均科技奖励 from (
select annual as 年份 ,company_name as 公司名称,award as 奖项,cast(award_amount as int)  as 奖励金额 
from   tech.ads_tech_details_technology_management t1
union all 
select annual as 年份 ,company_name as 公司名称,award as 奖项,cast(award_amount as int) as 奖励金额 
from   tech.ads_tech_details_technology_project t2) t3

--年均科技项目
select sum(t1.technology_number) as 科技总数
,count(distinct annual) as 年份数
,sum(t1.technology_number)/count(distinct annual)  as 年均科技项目数
from tech.ads_tech_science_technology_project_total_year t1 