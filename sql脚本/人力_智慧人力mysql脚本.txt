SELECT
	emp_custom_sort,
	CASE identity_type WHEN 0 THEN '身份证'WHEN 1 THEN '护照'ELSE '境外证件'END AS identity_type_str,
	identity_id,
	t.name,
	CASE gender WHEN 1 THEN '男' ELSE '女' END AS gender_str,
	birth,
	age,
	tall,
	blood_type,
	CASE nation WHEN 1 THEN '中国' ELSE '外国' END nation_str,
	jiguan,
	t.extend_field_1 AS birth_place_str,
	t.extend_field_2 AS registered_residence_str,
	e.name AS ethic_str,
	CASE marital_status WHEN 0 THEN '未婚' WHEN 1 THEN '已婚' WHEN 2 THEN '离异'END AS marital_status_str,
	CASE have_child WHEN 1 THEN '未育' WHEN 2 THEN '一孩' WHEN 3 THEN '二孩' WHEN 4 THEN '多孩' END have_child_str,
	pf.status AS politic_face_str,
	DATE_FORMAT(join_party_date, '%Y-%m-%d') AS join_party_date,
	local_party_branch,
	p.posts_name AS party_job_str,
	CASE SUBSTRING_INDEX(educationInfo, '$_$', 1) WHEN 0 THEN '高中及以下'
		WHEN 1 THEN '大专'
		WHEN 2 THEN '本科'
		WHEN 3 THEN '硕士研究生'
		WHEN 4 THEN '博士研究生' END education_str,
	CASE degree
	WHEN 0 THEN '无'
	WHEN 1 THEN '学士'
	WHEN 2 THEN '硕士'
	WHEN 3 THEN '博士'END degree_str,
SUBSTRING_INDEX(SUBSTRING_INDEX(educationInfo, '$_$', 2), '$_$',-1) graduate_from,
SUBSTRING_INDEX(SUBSTRING_INDEX(educationInfo, '$_$', 3), '$_$',-1) enddt,
SUBSTRING_INDEX(SUBSTRING_INDEX(educationInfo, '$_$', 4), '$_$',-1) edu_major,
CASE
	w.employment_category WHEN 1 THEN '合同工'
	WHEN 2 THEN '劳务派遣'
	WHEN 3 THEN '实习生'
	WHEN 4 THEN '返聘'
	WHEN 5 THEN '外聘顾问(专家)' END AS employment_category_str,
CASE
	w.job_status WHEN 1 THEN '在职'
	WHEN 2 THEN '离职'
	WHEN 3 THEN '退休'
	WHEN 4 THEN '在岗'
	WHEN 5 THEN '离岗' END AS job_status_str,
	DATE_FORMAT(work_date, '%Y-%m-%d') AS work_date,
	DATE_FORMAT(join_date, '%Y-%m-%d') AS join_date,
	phone,
	office_telephone,
	email,
	address,
	emerg_cont_person,
	emerg_cont_methon,
	emerg_cont_addr,
	w.company_code ,
	w.extend_field_2
	FROM
	hr_employee t
INNER JOIN (
SELECT
	dqrz.*
FROM
	hr_work_experience_inner dqrz
INNER JOIN (
	SELECT
		SUBSTRING_INDEX(GROUP_CONCAT(id ORDER BY startdt DESC, position_start_date DESC), ',', 1) id
	FROM
		hr_work_experience_inner
	WHERE
		table_status = 1
		AND job_type = 1
	GROUP BY
		employee_id,
		company_code
) t ON
	t.id = dqrz.id
UNION ALL
SELECT
	jz.*
FROM
	hr_work_experience_inner jz
INNER JOIN(
	SELECT
		dqrz.*
	FROM
		hr_work_experience_inner dqrz
	INNER JOIN (
		SELECT
			SUBSTRING_INDEX(GROUP_CONCAT(id ORDER BY startdt DESC, position_start_date DESC), ',', 1) id
		FROM
			hr_work_experience_inner
		WHERE
			table_status = 1
			AND job_type = 1
		GROUP BY
			employee_id,
			company_code
) t ON
		t.id = dqrz.id
) rz ON
	jz.employee_id = rz.employee_id
WHERE
	jz.job_type = 0
	AND jz.table_status = 1
	AND jz.startdt BETWEEN rz.startdt AND IFNULL(rz.enddt, '9999-12-31')
	AND jz.company_code != rz.company_code
) w ON w.employee_id = t.employee_id
LEFT JOIN hr_post_type pt ON pt.id = w.post_type
LEFT JOIN hr_post_level pl ON pl.id = w.post_level
LEFT JOIN (
SELECT
	e.employee_id,
	SUBSTRING_INDEX(GROUP_CONCAT(IFNULL(e.education,-1), '$_$', IFNULL(IF(e.graduate_from = '9999', e.graduate_from_other, c.name), ''), '$_$', IFNULL(e.enddt, '至今'), '$_$', IFNULL(e.edu_major, '') ORDER BY education DESC, startdt DESC, IFNULL(enddt, '9999-12-31') DESC SEPARATOR '$@$'), '$@$', 1) educationInfo
FROM
	hr_education_experience e
LEFT JOIN hr_college c ON
	c.coid = SUBSTRING_INDEX(e.graduate_from, ',',-1)
WHERE
	table_status = 1
GROUP BY
	employee_id
) edu ON
edu.employee_id = t.employee_id
LEFT JOIN (
SELECT
	employee_id,
	SUBSTRING_INDEX(GROUP_CONCAT(degree ORDER BY degree DESC SEPARATOR '$@$'), '$@$', 1) degree
FROM
	hr_education_experience e
WHERE
	table_status = 1
GROUP BY
	employee_id
) edu2 ON edu2.employee_id = t.employee_id
LEFT JOIN hr_ethic e ON e.ethic_id = t.ethic
LEFT JOIN hr_politic_face AS pf ON t.politic_face = pf.id
LEFT JOIN hr_party_posts p ON p.id = t.party_job
WHERE t.table_status = 1
AND w.job_status IN(1, 4)