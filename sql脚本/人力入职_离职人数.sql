




-- -----------------------------------------------------------------------------
-- ---------------------------------建表sql_ads---------------------------------
-- -----------------------------------------------------------------------------





-- 任期制与制约化

CREATE TABLE hr_ads.ads_hr_tenure_system_total_year (
	id serial4 NOT NULL,
	document_type varchar(200) NULL, -- 证件类型
	id_number varchar(200) NULL, -- 证件号
	"name" varchar(200) NOT NULL, -- 姓名
	credit_code varchar(200) NOT NULL, -- 公司统一信用代码
	company_name varchar(200) NOT NULL, -- 公司名称
	"position" varchar(200) NOT NULL, -- 职位
	term_start date NULL, -- 任期开始日期
	term_end date NULL, -- 任期结束日期
	term_agreement varchar(200) NULL, -- 任期协议
	annual_statement varchar(200) NULL, -- 年度责任书
	job_description varchar(200) NULL, -- 岗位说明书
	is_sign varchar(200) NULL, -- 是否签约
	create_time date NOT NULL DEFAULT now(), -- 创建时间
	url varchar NULL, -- 图片链接
	dept_id int8 NOT NULL, -- 公司id
	parent_id int8 NULL, -- 上级id
	CONSTRAINT ads_hr_tenure_system_total_year_pkey PRIMARY KEY (id)
);
COMMENT ON TABLE hr_ads.ads_hr_tenure_system_total_year IS '人才_任期制与制约化';

-- Column comments

COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year.document_type IS '证件类型';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year.id_number IS '证件号';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year."name" IS '姓名';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year.credit_code IS '公司统一信用代码';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year.company_name IS '公司名称';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year."position" IS '职位';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year.term_start IS '任期开始日期';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year.term_end IS '任期结束日期';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year.term_agreement IS '任期协议';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year.annual_statement IS '年度责任书';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year.job_description IS '岗位说明书';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year.is_sign IS '是否签约';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year.create_time IS '创建时间';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year.url IS '图片链接';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year.dept_id IS '公司id';
COMMENT ON COLUMN hr_ads.ads_hr_tenure_system_total_year.parent_id IS '上级id';












-- 人员状态变更明细-v1

-- CREATE TABLE hr_ads.ads_rpt_hr_compo_company_change_detail_df (
-- stat_year int4 NULL, -- 统计年份
-- stat_quarter date NULL, -- 统计季度
-- stat_type varchar(50) NULL, -- 统计类别
-- id int8 NULL, -- 序号
-- employee_id varchar(50) NULL, -- 关联员工
-- company_name varchar(255) NULL, -- 任职企业
-- company_code varchar(64) NULL, -- 公司id，关联组织机构表
-- company_level varchar(24) NULL, -- 企业组织级别 与机构树保持一致
-- entry_mode int4 NULL, -- 入职方式	  建表，关联表   社会招聘/院校招聘/内部调动/外部调动
-- update_attribute int4 NULL, -- 修改属性
-- employment_category int4 NULL, -- 用工类别	  建表，关联表 合同工/劳务派遣/实习生/返聘
-- position_start_date date NULL, -- 担任职位开始时间时间
-- position_end_date date NULL, -- 担任职位结束时间
-- job_status int4 NULL, -- 员工状态	1 在职 2 离职 3 退休 4 在岗 5 离岗
-- startdt date NULL, -- 企业任职开始时间 
-- enddt date NULL, -- 企业结束时间 当前未离职退休则为至今 或者 null
-- alents int4 NULL, -- 是否为高级人才  0-否 1-是 
-- post_type int4 NULL, -- 岗位类型  见信息模板-- 关联 岗位表
-- post_level int4 NULL, -- 层级 详情见信息模板-- 关联 层级表
-- position_attribute int4 NULL, -- 职位属性 1-高层管理 2-协管 3 其他
-- second_department varchar(100) NULL, -- 二级部门
-- three_department varchar(100) NULL, -- 三级部门
-- four_department varchar(100) NULL, -- 四级部门
-- five_department varchar(100) NULL, -- 五级部门
-- job_name varchar(100) NULL, -- 职务名称
-- job_type int4 NULL, -- 职委类型 1-任职 0-兼职
-- job_flag int4 NULL, -- 标记，关联任职与兼职 job_flag相同表示同一分组，然后根据职委类型判断兼职任职
-- part_time_num int4 NULL, -- 兼任企业数量 0-无兼任 1-一家兼任 2-两家兼任 3-三家兼任 4-四家兼任 5-五家兼任
-- table_date timestamp NULL, -- 修改和删除，都是重新添加一条记录
-- table_status int4 NULL, -- 状态 0 删除 1 可用 2 修改 3 彻底删除 4 5
-- extend_field_1 varchar(255) NULL, -- 扩展字段1
-- extend_field_2 varchar(255) NULL, -- 扩展字段2
-- extend_field_3 int4 NULL, -- 扩展字段3
-- extend_field_4 int4 NULL, -- 扩展字段4
-- position_rank int4 NULL, -- 员工职位经历排序
-- position_reverse_rank int4 NULL, -- 员工职位经历逆向排序
-- company_start_date date NULL, -- 企业任职开始时间_清洗后
-- company_end_date date NULL, -- 企业任职结束时间_清洗后
-- etl_date date NOT NULL DEFAULT CURRENT_DATE -- 调度更新时间
-- );
-- COMMENT ON TABLE hr_ads.ads_rpt_hr_compo_company_change_detail_df IS '人员状态变更明细';





-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.stat_year IS '统计年份';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.stat_quarter IS '统计季度';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.stat_type IS '统计类别';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.id IS '经历序号';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.employee_id IS '关联员工';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.company_name IS '智慧人力任职企业';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.company_code IS '智慧人力公司id';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.company_level IS '智慧人力企业组织级别';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.entry_mode IS '入职方式';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.update_attribute IS '修改属性';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.employment_category IS '用工类别';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.position_start_date IS '担任职位开始时间时间';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.position_end_date IS '担任职位结束时间';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.job_status IS '员工状态';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.startdt IS '企业任职开始时间 ';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.enddt IS '企业任职结束时间';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.alents IS '是否为高级人才';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.post_type IS '岗位类型';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.post_level IS '岗位层级';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.position_attribute IS '职位属性';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.second_department IS '二级部门';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.three_department IS '三级部门';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.four_department IS '四级部门';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.five_department IS '五级部门';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.job_name IS '职务名称';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.job_type IS '职委类型';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.job_flag IS '职位标记';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.part_time_num IS '兼任企业数量';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.table_date IS '记录修改日期';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.table_status IS '记录状态';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.extend_field_1 IS '扩展字段1';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.extend_field_2 IS '扩展字段2';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.extend_field_3 IS '扩展字段3';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.extend_field_4 IS '扩展字段4';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.position_rank IS '员工职位经历排序';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.position_reverse_rank IS '员工职位经历逆向排序';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.company_start_date IS '企业任职开始时间_清洗后';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.company_end_date IS '企业任职结束时间_清洗后';
-- COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.etl_date IS '调度更新时间';













-- 人员状态变更明细-v2

CREATE TABLE hr_ads.ads_rpt_hr_compo_company_change_detail_df (
	stat_year int4 NULL, -- 统计年份
	stat_quarter date NULL, -- 统计季度
	stat_type varchar(50) NULL, -- 统计类别
	employee_id varchar(50) NULL, -- 员工编号
	emp_code varchar NULL, -- 公司人力编码
	company_code int4 NULL, -- 公司产权序号
	parent_id int8 NULL, -- 企业父编码
	abbreviation varchar NULL, -- 企业简称
	company_name varchar NULL, -- 公司名称
	emp_name varchar(255) NULL, -- 姓名
	gender text NULL, -- 性别
	education varchar NULL, -- 学历
	ethic varchar(24) NULL, -- 民族
	have_child varchar(24) NULL, -- 生育情况
	graduate_name varchar(200) NULL, -- 毕业学校
	graduate_type text NULL, -- 学校类型
	post_type_name varchar NULL, -- 岗位类型
	parent_post_type_name bpchar NULL, -- 岗位大类
	profession varchar NULL, -- 职业级别
	professional_title varchar NULL, -- 职称级别
	birth date NULL, -- 出生日期
	join_date date NULL, -- 入职日期
	work_date date NULL, -- 参加工作日期
	position_start_date date NULL, -- 担任职位开始时间
	position_end_date date NULL, -- 担任职位结束时间
	job_status varchar(50) NULL, -- 员工状态	1 在职 2 离职 3 退休 4 在岗 5 离岗
	age int4 NULL, -- 年龄
	join_age float8 NULL, -- 司龄
	work_age float8 NULL, -- 工龄
	age_grp text NULL, -- 年龄
	join_age_grp text NULL, -- 司龄段
	work_age_grp text NULL, -- 工龄段
	administrative_type varchar(24) NULL, -- 行政类型
	employment_category_name varchar(24) NULL, -- 用工类别
	country varchar NULL, -- 国家
	province varchar NULL, -- 省份
	city varchar NULL, -- 地市
	county varchar NULL, -- 区县
	longitude varchar NULL, -- 经度
	latitude varchar NULL, -- 维度
	email varchar(60) NULL, -- 邮箱
	image_path varchar(255) NULL, -- 头像
	politic_face text NULL, -- 政治面貌
	local_party_branch varchar(80) NULL, -- 所属党支部
	alents text NULL, -- 是否高级人员
	department varchar(100) NULL, -- 部门
	job_name varchar(100) NULL, -- 职务
	entry_mode_name varchar(24) NULL, -- 招聘方式
	identity_id varchar(50) NULL, -- 证件号
	phone varchar(30) NULL, -- 手机号
	order_sort int8 NULL, -- 排序号
	marital_status varchar NULL, -- 婚姻情况 0 未婚 1 已婚 2 离异
	position_rank int4 NULL, -- 员工职位经历排序
	position_reverse_rank int4 NULL, -- 员工职位经历逆向排序
	company_start_date date NULL, -- 企业任职开始时间_清洗后
	company_end_date date NULL, -- 企业任职结束时间_清洗后
	etl_date date NOT NULL DEFAULT CURRENT_DATE -- 调度更新时间
);
COMMENT ON TABLE hr_ads.ads_rpt_hr_compo_company_change_detail_df IS '员工状态变更明细宽表';



-- Column comments

COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.stat_year IS '统计年份';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.stat_quarter IS '统计季度';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.stat_type IS '统计类别';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.employee_id IS '员工编号';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.emp_code IS '公司人力编码';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.company_code IS '公司产权序号';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.parent_id IS '企业父编码';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.abbreviation IS '企业简称';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.company_name IS '公司名称';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.emp_name IS '姓名';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.gender IS '性别';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.education IS '学历';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.ethic IS '民族';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.have_child IS '生育情况';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.graduate_name IS '毕业学校';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.graduate_type IS '学校类型';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.post_type_name IS '岗位类型';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.parent_post_type_name IS '岗位大类';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.profession IS '职业级别';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.professional_title IS '职称级别';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.birth IS '出生日期';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.join_date IS '入职日期';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.work_date IS '参加工作日期';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.position_start_date IS '担任职位开始时间时间';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.position_end_date IS '担任职位结束时间';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.job_status IS '员工状态	1 在职 2 离职 3 退休 4 在岗 5 离岗';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.age IS '年龄';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.join_age IS '司龄';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.work_age IS '工龄';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.age_grp IS '年龄';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.join_age_grp IS '司龄段';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.work_age_grp IS '工龄段';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.administrative_type IS '行政类型';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.employment_category_name IS '用工类别';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.country IS '国家';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.province IS '省份';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.city IS '地市';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.county IS '区县';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.longitude IS '经度';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.latitude IS '维度';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.email IS '邮箱';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.image_path IS '头像';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.politic_face IS '政治面貌';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.local_party_branch IS '所属党支部';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.alents IS '是否高级人员';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.department IS '部门';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.job_name IS '职务';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.entry_mode_name IS '招聘方式';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.identity_id IS '证件号';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.phone IS '手机号';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.order_sort IS '排序号';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.marital_status IS '-- 婚姻情况 0 未婚 1 已婚 2 离异';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.position_rank IS '员工职位经历排序';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.position_reverse_rank IS '员工职位经历逆向排序';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.company_start_date IS '企业任职开始时间_清洗后';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.company_end_date IS '企业任职结束时间_清洗后';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_company_change_detail_df.etl_date IS '调度更新时间';



















-- 人员状态变更汇总


CREATE TABLE hr_ads.ads_rpt_hr_compo_presonnel_change_df (
	stat_year varchar(50) NULL,
	stat_quarter date NULL,
	join_employee_cnt int8 NULL,
	join_employee_cnt_qoq int8 NULL,
	join_employee_cnt_yoy int8 NULL,
	join_employee_cnt_qoq_rate numeric(6,3) NULL,
	join_employee_cnt_yoy_rate numeric(6,3) NULL,
	leave_employee_cnt int8 NULL,
	leave_employee_cnt_qoq int8 NULL,
	leave_employee_cnt_yoy int8 NULL,
	leave_employee_cnt_qoq_rate numeric(6,3) NULL,
	leave_employee_cnt_yoy_rate numeric(6,3) NULL,
	on_job_employee_cnt int8 NULL,
	on_job_employee_cnt_qoq int8 NULL,
	on_job_employee_cnt_yoy int8 NULL,
	on_job_employee_cnt_qoq_rate numeric(6,3) NULL,
	on_job_employee_cnt_yoy_rate numeric(6,3) NULL,
	etl_date date NOT NULL DEFAULT CURRENT_DATE
);
COMMENT ON TABLE hr_ads.ads_rpt_hr_compo_presonnel_change_df IS '人员变更记录';



COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.stat_year IS '统计年份';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.stat_quarter IS '统计季度';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.join_employee_cnt IS '入职人数';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.join_employee_cnt_qoq IS '入职人数_上一季度';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.join_employee_cnt_yoy IS '入职人数_上年同期';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.join_employee_cnt_qoq_rate IS '入职人数_季度环比';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.join_employee_cnt_yoy_rate IS '入职人数_季度同比';

COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.leave_employee_cnt IS '离职人数';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.leave_employee_cnt_qoq IS '离职人数_上一季度';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.leave_employee_cnt_yoy IS '离职人数_上年同期';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.leave_employee_cnt_qoq_rate IS '离职人数_季度环比';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.leave_employee_cnt_yoy_rate IS '离职人数_季度同比';

COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.on_job_employee_cnt IS '在职人数';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.on_job_employee_cnt_qoq IS '在职人数_上一季度';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.on_job_employee_cnt_yoy IS '在职人数_上年同期';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.on_job_employee_cnt_qoq_rate IS '在职人数_季度环比';
COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.on_job_employee_cnt_yoy_rate IS '在职人数_季度同比';

COMMENT ON COLUMN hr_ads.ads_rpt_hr_compo_presonnel_change_df.etl_date IS '更新时间';



















-- -----------------------------------------------------------------------------
-- -----------------------------------计算sql-----------------------------------
-- -----------------------------------------------------------------------------







-- 人员状态变更明细


-- truncate hr_ads.ads_rpt_hr_compo_company_change_detail_df

insert into hr_ads.ads_rpt_hr_compo_company_change_detail_df

select *
from hr_dwd.dwd_hr_compo_company_change_wt_df
where 1=1
and to_char(stat_quarter, 'yyyy-MM-dd') = '2022-04-01'












-- -- 人员状态变更汇总-v1


-- -- truncate hr_ads.ads_rpt_hr_compo_presonnel_change_df

-- insert into hr_ads.ads_rpt_hr_compo_presonnel_change_df

-- with ep as 
-- (
-- 	select employee_id,
-- 			position_start_date,
-- 			position_end_date,
-- 			job_status,
-- 			position_rank,
-- 			first_value(position_start_date) over (
-- 				partition by employee_id order by position_rank asc) as company_start_date,
-- 			first_value(position_end_date) over (
-- 				partition by employee_id order by position_rank desc) as company_end_date
-- 	from 
-- 	(
-- 		select employee_id,
-- 				position_start_date,
-- 				position_end_date,
-- 				job_status,
-- 				row_number() over (partition by employee_id order by position_start_date asc) as position_rank
-- 		from hr_dai.hr_work_experience_inner
-- 		where 1=1
-- 		and table_status=1
-- 		and position_start_date is not null
-- 	) tbl
-- 	where 1=1
-- 	order by employee_id asc,
-- 			position_rank asc
-- )



-- select stat_year,
-- 		stat_quarter,
-- 		join_employee_cnt,
-- 		join_employee_cnt_qoq,
-- 		join_employee_cnt_yoy,
-- 		case when join_employee_cnt_qoq > 0 
-- 			then (join_employee_cnt - join_employee_cnt_qoq) * 1.0 / join_employee_cnt_qoq
-- 			else 0 end as join_employee_cnt_qoq_rate,
-- 		case when join_employee_cnt_yoy > 0 
-- 			then (join_employee_cnt - join_employee_cnt_yoy) * 1.0 / join_employee_cnt_yoy
-- 			else 0 end as join_employee_cnt_yoy_rate,
-- 		leave_employee_cnt,
-- 		leave_employee_cnt_qoq,
-- 		leave_employee_cnt_yoy,
-- 		case when leave_employee_cnt_qoq > 0 
-- 			then (leave_employee_cnt - leave_employee_cnt_qoq) * 1.0 / leave_employee_cnt_qoq
-- 			else 0 end as leave_employee_cnt_qoq_rate,
-- 		case when leave_employee_cnt_yoy > 0 
-- 			then (leave_employee_cnt - leave_employee_cnt_yoy) * 1.0 / leave_employee_cnt_yoy
-- 			else 0 end as leave_employee_cnt_yoy_rate,
-- 		on_job_employee_cnt,
-- 		on_job_employee_cnt_qoq,
-- 		on_job_employee_cnt_yoy,
-- 		case when on_job_employee_cnt_qoq > 0 
-- 			then (on_job_employee_cnt - on_job_employee_cnt_qoq) * 1.0 / on_job_employee_cnt_qoq
-- 			else 0 end as on_job_employee_cnt_qoq_rate,
-- 		case when on_job_employee_cnt_yoy > 0 
-- 			then (on_job_employee_cnt - on_job_employee_cnt_yoy) * 1.0 / on_job_employee_cnt_yoy
-- 			else 0 end as on_job_employee_cnt_yoy_rate
-- from 
-- (
-- 	select je.stat_year,
-- 			je.stat_quarter,
-- 			je.join_employee_cnt,
-- 			le.leave_employee_cnt,
-- 			oe.on_job_employee_cnt,
-- 			lag(je.join_employee_cnt) over (order by je.stat_year asc, je.stat_quarter asc) as join_employee_cnt_qoq,
-- 			lag(je.join_employee_cnt, 4) over (order by je.stat_year asc, je.stat_quarter asc) as join_employee_cnt_yoy,
-- 			lag(le.leave_employee_cnt) over (order by je.stat_year asc, je.stat_quarter asc) as leave_employee_cnt_qoq,
-- 			lag(le.leave_employee_cnt, 4) over (order by je.stat_year asc, je.stat_quarter asc) as leave_employee_cnt_yoy,
-- 			lag(oe.on_job_employee_cnt) over (order by je.stat_year asc, je.stat_quarter asc) as on_job_employee_cnt_qoq,
-- 			lag(oe.on_job_employee_cnt, 4) over (order by je.stat_year asc, je.stat_quarter asc) as on_job_employee_cnt_yoy
-- 	from
-- 	(
-- 		select extract('year' from ep.company_start_date) as stat_year,
-- 				cast(date_trunc('quarter', ep.company_start_date) as date) as stat_quarter,
-- 				count(distinct employee_id) as join_employee_cnt
-- 		from ep
-- 		where 1=1
-- 		and to_char(ep.company_start_date, 'yyyy-MM-dd') between '2020-07-01' and '2022-06-30'
-- 		group by extract('year' from ep.company_start_date),
-- 				cast(date_trunc('quarter', ep.company_start_date) as date)
-- 	) je
-- 	left join
-- 	(
-- 		select extract('year' from ep.company_end_date) as stat_year,
-- 				cast(date_trunc('quarter', ep.company_end_date) as date) as stat_quarter,
-- 				count(distinct employee_id) as leave_employee_cnt
-- 		from ep
-- 		where 1=1
-- 		and to_char(ep.company_end_date, 'yyyy-MM-dd') between '2020-07-01' and '2022-06-30'
-- 		group by extract('year' from ep.company_end_date),
-- 				cast(date_trunc('quarter', ep.company_end_date) as date)
-- 	) le on je.stat_year = le.stat_year and je.stat_quarter = le.stat_quarter
-- 	left join
-- 	(
-- 		select dd.stat_year,
-- 				dd.stat_quarter,
-- 				count(distinct ep.employee_id) as on_job_employee_cnt
-- 		from
-- 		(
-- 			select extract('year' from date) as stat_year,
-- 					cast(date_trunc('quarter', date) as date) as stat_quarter,
-- 					cast(date_trunc('month', date) as date) as stat_month,
-- 					row_number() over (partition by cast(date_trunc('quarter', date) as date)
-- 						order by cast(date_trunc('month', date) as date) desc) as month_rank,
-- 					min(date) as min_date,
-- 					max(date) as max_date
-- 			from dim.dim_date dd
-- 			where 1=1
-- 			and to_char(date, 'yyyy-MM-dd') between '2020-07-01' and '2022-06-30'
-- 			group by extract('year' from date),
-- 					cast(date_trunc('quarter', date) as date),
-- 					cast(date_trunc('month', date) as date)
-- 		) dd
-- 		cross join ep
-- 		where 1=1
-- 		and month_rank = 1
-- 		and ep.position_start_date <= dd.max_date
-- 		and (ep.position_end_date is null or ep.position_end_date >= dd.min_date)
-- 		group by dd.stat_year,
-- 				dd.stat_quarter
-- 	) oe on je.stat_year = oe.stat_year and je.stat_quarter = oe.stat_quarter
-- 	where 1=1
-- 	order by je.stat_year asc,
-- 			je.stat_quarter asc
-- ) tbl















-- 人员状态变更汇总-v2


-- truncate hr_ads.ads_rpt_hr_compo_presonnel_change_df

insert into hr_ads.ads_rpt_hr_compo_presonnel_change_df

select stat_year,
		stat_quarter,
		join_employee_cnt,
		join_employee_cnt_qoq,
		join_employee_cnt_yoy,
		case when join_employee_cnt_qoq > 0 
			then (join_employee_cnt - join_employee_cnt_qoq) * 1.0 / join_employee_cnt_qoq
			else 0 end as join_employee_cnt_qoq_rate,
		case when join_employee_cnt_yoy > 0 
			then (join_employee_cnt - join_employee_cnt_yoy) * 1.0 / join_employee_cnt_yoy
			else 0 end as join_employee_cnt_yoy_rate,
		leave_employee_cnt,
		leave_employee_cnt_qoq,
		leave_employee_cnt_yoy,
		case when leave_employee_cnt_qoq > 0 
			then (leave_employee_cnt - leave_employee_cnt_qoq) * 1.0 / leave_employee_cnt_qoq
			else 0 end as leave_employee_cnt_qoq_rate,
		case when leave_employee_cnt_yoy > 0 
			then (leave_employee_cnt - leave_employee_cnt_yoy) * 1.0 / leave_employee_cnt_yoy
			else 0 end as leave_employee_cnt_yoy_rate,
		on_job_employee_cnt,
		on_job_employee_cnt_qoq,
		on_job_employee_cnt_yoy,
		case when on_job_employee_cnt_qoq > 0 
			then (on_job_employee_cnt - on_job_employee_cnt_qoq) * 1.0 / on_job_employee_cnt_qoq
			else 0 end as on_job_employee_cnt_qoq_rate,
		case when on_job_employee_cnt_yoy > 0 
			then (on_job_employee_cnt - on_job_employee_cnt_yoy) * 1.0 / on_job_employee_cnt_yoy
			else 0 end as on_job_employee_cnt_yoy_rate
from 
(
	select je.stat_year,
			je.stat_quarter,
			je.join_employee_cnt,
			le.leave_employee_cnt,
			oe.on_job_employee_cnt,
			lag(je.join_employee_cnt) over (order by je.stat_year asc, je.stat_quarter asc) as join_employee_cnt_qoq,
			lag(je.join_employee_cnt, 4) over (order by je.stat_year asc, je.stat_quarter asc) as join_employee_cnt_yoy,
			lag(le.leave_employee_cnt) over (order by je.stat_year asc, je.stat_quarter asc) as leave_employee_cnt_qoq,
			lag(le.leave_employee_cnt, 4) over (order by je.stat_year asc, je.stat_quarter asc) as leave_employee_cnt_yoy,
			lag(oe.on_job_employee_cnt) over (order by je.stat_year asc, je.stat_quarter asc) as on_job_employee_cnt_qoq,
			lag(oe.on_job_employee_cnt, 4) over (order by je.stat_year asc, je.stat_quarter asc) as on_job_employee_cnt_yoy
	from
	(
		select cc.stat_year,
				cc.stat_quarter,
				count(distinct employee_id) as join_employee_cnt
		from hr_dwd.dwd_hr_compo_company_change_wt_df cc
		where 1=1
		and stat_type = '入职'
		group by cc.stat_year,
				cc.stat_quarter
	) je
	left join
	(
		select cc.stat_year,
				cc.stat_quarter,
				count(distinct employee_id) as leave_employee_cnt
		from hr_dwd.dwd_hr_compo_company_change_wt_df cc
		where 1=1
		and stat_type = '离职'
		group by cc.stat_year,
				cc.stat_quarter
	) le on je.stat_year = le.stat_year and je.stat_quarter = le.stat_quarter
	left join
	(
		select cc.stat_year,
				cc.stat_quarter,
				count(distinct employee_id) as on_job_employee_cnt
		from hr_dwd.dwd_hr_compo_company_change_wt_df cc
		where 1=1
		and stat_type = '在职'
		group by cc.stat_year,
				cc.stat_quarter
	) oe on je.stat_year = oe.stat_year and je.stat_quarter = oe.stat_quarter
	where 1=1
	order by je.stat_year asc,
			je.stat_quarter asc
) tbl

