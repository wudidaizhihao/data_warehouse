




-- 产权树父节点遍历


create table dim.property_department_bi as
select
	pd5.dept_id ,
	pd1 .dept_name  dept_name1,
	pd2 .dept_name  dept_name2,
	pd3 .dept_name  dept_name3,
	pd4 .dept_name dept_name4,
	pd5 .dept_name  dept_name5 
from
	property_department pd5
	left join property_department pd4 on pd4 .dept_id  =pd5 .parent_id 
	left join property_department pd3 on pd3 .dept_id  =pd4.parent_id 
	left join property_department pd2 on pd2 .dept_id  =pd3.parent_id 
	left join property_department pd1 on pd1 .dept_id  =pd2.parent_id 
	where pd5."level"  = 5 

union  all

select
	pd4.dept_id ,
	pd1 .dept_name  dept_name1,
	pd2 .dept_name  dept_name2,
	pd3 .dept_name  dept_name3,
	pd4 .dept_name dept_name4,
	'' dept_name5
from
	property_department pd4
	left join property_department pd3 on pd3 .dept_id  =pd4.parent_id 
	left join property_department pd2 on pd2 .dept_id  =pd3.parent_id 
	left join property_department pd1 on pd1 .dept_id  =pd2.parent_id 
	where pd4."level"  = 4 

union all 
select
	pd3.dept_id ,
	pd1 .dept_name  dept_name1,
	pd2 .dept_name  dept_name2,
	pd3 .dept_name  dept_name3,
	'' dept_name4,
	'' dept_name5
from
	property_department pd3
	left join property_department pd2 on pd2 .dept_id  =pd3.parent_id 
	left join property_department pd1 on pd1 .dept_id  =pd2.parent_id 
	where pd3."level"  = 3 

union all 

select
	pd2.dept_id ,
	pd1 .dept_name  dept_name1,
	pd2 .dept_name  dept_name2,
	'' dept_name3,
	'' dept_name4,
	'' dept_name5
from
	property_department pd2
	left join property_department pd1 on pd1 .dept_id  =pd2.parent_id 
	where pd2."level"  = 2 









-- 产权树父子节点关系递归


CREATE TABLE dim.dim_property_department_relation_recurve AS
SELECT *
FROM
    (SELECT cast((parent_dept_id).value as int) AS parent_dept_id,
                             cast(substr(reverse((parent_dept_id).key), 1, 1) AS int) AS parent_dept_level,
                             child_dept_id,
                             child_dept_level
     FROM
         (SELECT dept_id AS child_dept_id,
                 level AS child_dept_level,
                          json_each_text(json_build_object('parent_dept_id_4',"parent_dept_id_4",'parent_dept_id_3',"parent_dept_id_3",'parent_dept_id_2',"parent_dept_id_2",'parent_dept_id_1',"parent_dept_id_1"))AS parent_dept_id
          FROM
              (SELECT pd5.dept_id,
                      pd5.level,
                      pd4.dept_id AS parent_dept_id_4,
                      pd3.dept_id AS parent_dept_id_3,
                      pd2.dept_id AS parent_dept_id_2,
                      pd1.dept_id AS parent_dept_id_1
               FROM dim.dim_property_department pd5
               LEFT JOIN dim.dim_property_department pd4 ON pd4 .dept_id =pd5 .parent_id
               LEFT JOIN dim.dim_property_department pd3 ON pd3 .dept_id =pd4.parent_id
               LEFT JOIN dim.dim_property_department pd2 ON pd2 .dept_id =pd3.parent_id
               LEFT JOIN dim.dim_property_department pd1 ON pd1 .dept_id =pd2.parent_id
               WHERE pd5.level = 5
                   UNION ALL
                   SELECT pd4.dept_id,
                          pd4.level,
                          0 AS parent_dept_id_4,
                          pd3.dept_id AS parent_dept_id_3,
                          pd2.dept_id AS parent_dept_id_2,
                          pd1.dept_id AS parent_dept_id_1
                   FROM dim.dim_property_department pd4
                   LEFT JOIN dim.dim_property_department pd3 ON pd3 .dept_id =pd4.parent_id
                   LEFT JOIN dim.dim_property_department pd2 ON pd2 .dept_id =pd3.parent_id
                   LEFT JOIN dim.dim_property_department pd1 ON pd1 .dept_id =pd2.parent_id WHERE pd4.level = 4
                   UNION ALL
                   SELECT pd3.dept_id,
                          pd3.level,
                          0 AS parent_dept_id_4,
                          0 AS parent_dept_id_3,
                          pd2.dept_id AS parent_dept_id_2,
                          pd1.dept_id AS parent_dept_id_1
                   FROM dim.dim_property_department pd3
                   LEFT JOIN dim.dim_property_department pd2 ON pd2 .dept_id =pd3.parent_id
                   LEFT JOIN dim.dim_property_department pd1 ON pd1 .dept_id =pd2.parent_id WHERE pd3.level = 3
                   UNION ALL
                   SELECT pd2.dept_id,
                          pd2.level,
                          0 AS parent_dept_id_4,
                          0 AS parent_dept_id_3,
                          0 AS parent_dept_id_2,
                          pd1.dept_id AS parent_dept_id_1
                   FROM dim.dim_property_department pd2
                   LEFT JOIN dim.dim_property_department pd1 ON pd1 .dept_id =pd2.parent_id WHERE pd2.level = 2 ) tbl)tbl) tbl
WHERE 1=1
    AND parent_dept_level < child_dept_level









