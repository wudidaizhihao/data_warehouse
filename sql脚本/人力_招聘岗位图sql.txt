###########月度招聘
##根据名称，匹配统一信用代码
update hr.ads_hr_recruiting_position_total_year_his 
set credit_code  = t2 .credit_code 
from dim.dim_property_department t2
where hr.ads_hr_recruiting_position_total_year_his.company  = t2.dept_name 

##根据简称，补充统一信用代码
update hr.ads_hr_recruiting_position_total_year_his 
set credit_code  = t2 .credit_code 
from dim.dim_property_department t2
where hr.ads_hr_recruiting_position_total_year_his.company  = t2.abbreviation  
and hr.ads_hr_recruiting_position_total_year_his.credit_code  is null

## 手工匹配部分公司
select * from  hr.ads_hr_recruiting_position_total_year_his  t1 where t1.credit_code  is null

##有限匹配有本部的公司
update hr.ads_hr_recruiting_position_total_year_his  
set code  = t2.code 
,dept_id  = t2.dept_id 
,parent_id  = t2.parent_id 
from 
(select * from (
select  t1.report_type ,t1.dept_id ,t1.credit_code ,t1.parent_id  ,code ,
row_number () OVER(partition by credit_code order by report_type asc ) num
from dim.dim_property_department   t1 ) t2 where t2.num  = 1) t2
where  hr.ads_hr_recruiting_position_total_year_his.credit_code  = t2.credit_code 


###########月度人才引进
select * from (
select  t1.report_type ,t1.dept_id ,t1.credit_code ,t1.parent_id  ,code ,
row_number () OVER(partition by credit_code order by report_type asc ) num
from dim.dim_property_department   t1 ) t2 where t2.num  = 1

select * from hr.ads_hr_recruiting_position_total_year_his t1 

##根据名称，匹配统一信用代码
update hr.ads_hr_recruit_talent_introduction_mf 
set credit_code  = t2 .credit_code 
from dim.dim_property_department t2
where hr.ads_hr_recruit_talent_introduction_mf.company  = t2.dept_name 

##根据简称，补充统一信用代码
update hr.ads_hr_recruit_talent_introduction_mf 
set credit_code  = t2 .credit_code 
from dim.dim_property_department t2
where hr.ads_hr_recruit_talent_introduction_mf.company  = t2.abbreviation  
and hr.ads_hr_recruit_talent_introduction_mf.credit_code  is null

## 手工匹配部分公司
select company ,credit_code,annual  from  hr.ads_hr_recruit_talent_introduction_mf  t1 where t1.dept_id  is null

##有限匹配有本部的公司
update hr.ads_hr_recruit_talent_introduction_mf  
set code  = t2.code 
,dept_id  = t2.dept_id 
,parent_id  = t2.parent_id 
from 
(select * from (
select  t1.report_type ,t1.dept_id ,t1.credit_code ,t1.parent_id  ,code ,
row_number () OVER(partition by credit_code order by report_type asc ) num
from dim.dim_property_department   t1 ) t2 where t2.num  = 1) t2
where  hr.ads_hr_recruit_talent_introduction_mf.credit_code  = t2.credit_code 